---
layout: job_family_page
title: "Frontend Engineer"
---

{: .text-center}
<br>

## A brief overview:

GitLab is building an open source, single application for the entire DevOps lifecycle—from project planning and source code management to CI/CD, monitoring, and security.

We already have a large team of Frontend Engineers and we're planning to more than double the team's size over the next year. You can look over our organizational chart to see open vacancies.

At GitLab, we live and breathe open source principles. This means our entire handbook is online, and with a few clicks you can find the details of upcoming releases and an overview of the product vision you’d contribute to when working here.

What you can expect in a Frontend Engineer role at GitLab:

* You'll help improve the overall experience of our product through improving the quality of the Frontend features
* You’ll help identify areas of improvements in the code base and help contribute to make it better
* You’ll work alongside the UX team, the Backend Engineering team and Product Managers to iterate on new features within the GitLab product.
* You’ll learn, collaborate and teach other Frontend Engineers. Everyone can contribute something new to the team regardless of how long they’ve been in the industry.
* You’ll fix prioritized issues from the issue tracker. These are typically bugs listed in a GitLab issue with an attached severity and priority label.
* You'll write high quality code to complete scheduled direction issues assigned by your frontend engineering manager.
* You’ll contribute to and follow our workflow process, alongside the rest of the Frontend team and the GitLab community as a whole.

## Teams you might be a part of: 

We have vacancies across our Create, Secure, Defend, Monitor, Package, Configure & Serverless, Release & Verify, Geo, Fulfillment, Ecosystem, and Distribution stage groups. We work using Agile methodologies and ship features monthly.

Levels we are currently hiring for:

* Intermediate Frontend Engineers
* Senior Frontend Engineers

## You should apply if: 

* You have prior experience building Frontend web applications
* You have a solid understanding in core web and browser concepts (eg. how does JavaScript handle asynchronous code)
* You have working knowledge of when and how to make your code scale effectively
* You are proactive in taking ownership of tasks and take them to completion
* You desire to learn and continually give and receive feedback effectively
* You have a solid understanding of CSS, semantic HTML and core JavaScript concepts.
* Our values of collaboration, results, efficiency, diversity, iteration, and transparency (CREDIT) resonate with you.

## Nice to have: 
* You have experience contributing to open source software
* You have experience optimizing code and look for ways to resolve performance issues
* You have experience working with modern frontend frameworks (eg. React, Vue..etc)
* You have working knowledge of Ruby on Rails
* You have domain knowledge relevant to the product stage in which you are looking to join (eg. someone with CI/CD experience applying for the Verify & Release team)

## What it’s like to work here at GitLab:
The culture here at GitLab is something we’re incredibly proud of. Because GitLab team-members are currently located in 51 different countries, you’ll spend your time collaborating with kind, talented, and motivated colleagues from across the globe.

Some of the benefits you’ll be entitled to vary by the region or country you’re in. However, all GitLab team-members are fully remote and receive a "no ask, must tell" paid-time-off policy, where we don’t count the number of days you take off annually. You can work incredibly flexible hours, enabled by our asynchronous approach to communication. Also, every nine months or so, we’ll invite you to our Contribute event.

## How we define our levels

### Junior Frontend Engineer
> Typically equates to less than 2 years of experience

* Technical Skills
  * Needs guidance writing modular and maintainable code
  * Has less experience with HTML, CSS & JavaScript
* Code quality
  * Leaves code in substantially better shape than before
  * Needs prompting to fix bugs/regressions
* Communication
  * Needs help to manage time effectively
  * Participates in frontend technical conversations
* Performance & Scalability
  * Needs help writing production-ready code
  * Has little to no experience writing large scale apps

### Intermediate Frontend Engineer
> Typically equates to 2 to 4 years of experience

* Technical Skills
  * Needs minimal guidance to write modular, well-tested, and maintainable code
* Leadership
  * Propose new ideas, needs guidance in performing feasibility analyses and scoping the work
  * Capable of leading medium sized features, bug fixes, and integrations with limited guidance from leads or seniors (Eg. Begin to show architectural perspective)
  * Should be striving to make the entire team more productive through their efforts. This might be through mentoring junior developers, or by improving the team's process, documentation, testing or tooling in a way that helps everyone in the team be more effective.
* Code quality
  * Leave code in better shape than before (Eg. Leave tests in better shape than before: faster, more reliable, more comprehensive, etc.)
    * Strive to reduce complexity contained as much as possible: if a feature is complex, that's sometimes unavoidable, but Intermediate Developers show that they can reduce or mitigate complexity in their changes
  * Fix bugs/regressions without much prompting from team
  * Monitor overall code quality/build failures
* Communication
  * Provide thorough and timely code feedback for peers, generally leaving minimal work for later reviews / maintainers
  * Able to communicate on technical topics and present ideas to the rest of the team
  * Proactive in asking for guidance from others when needed 
  * Keep issues up-to-date with progress 
* Performance & Scalabilty
  * Writes production-ready code with minimal assistance
  * Fixes performance issues on GitLab.com with minimal guidance using our existing tools, and identify areas of improvements where needed

### Senior Frontend Engineer
> Typically equates to 4+ years of experience

* Technical Skills
  * Write modular, well-tested, and maintainable code
* Leadership
  * Propose new ideas, performing feasibility analyses and scoping the work
  * Capable of leading complicated features, bug fixes, and integrations with limited guidance from leads (Eg. Begin to show architectural perspective)
  * Should make the entire team more productive through their efforts. This might be through mentoring junior developers, or by improving the team's process, documentation, testing or tooling in a way that helps everyone in the team be more effective.
* Code quality
  * Leave code in substantially better shape than before (Eg. Keep complexity contained as much as possible, Leave tests in better shape than before)
  * Fix bugs/regressions quickly
  * Monitor overall code quality/build failures
* Communication
  * Provide thorough and timely code feedback for peers, leaving minimal work for later reviews / maintainers
  * Able to communicate clearly on technical topics and present ideas to the rest of the team
  * Keep issues up-to-date with progress
* Performance & Scalability
  * Excellent at writing production-ready code with little assistance
  * Are able to fix performance issues on GitLab.com with minimal guidance using our existing tools, and improve those tools where needed

## Team Specialties
As a frontend engineer in the following team specialty, you should expect to ...

### Manage
Increase user productivity by improving the user experience across the entire GitLab UI,
driving initiatives such as internationalization and analytics, as well as refreshing the design across well-trafficked parts of the UI.

### Plan
Enable people in any size organization the ability to manage and analyze projects, from ideation to execution.
You will work on our project management applications, including Issue Management, Kanban Boards, and Agile Portfolio Management.

### Create
Build and improve features related to creating projects using GitLab. This includes dealing with Source Code of projects including Snippets, Code Reviewing (Merge Requests), the Web IDE, projects' Wiki and Design Management.

### Verify
Focus on building functionality related to the Continuous Integration stage of the DevOps lifecycle. This includes contributing to Performance and Testing tools, Cross-Project Pipelines, Visual Review Apps, and CI Recipes. Our mission is to help developers feel confident in delivering their code to production.

### Package
Create a consistent user experience for developers to interact with our integrated universal package management solution. Our focus is to simplify package sharing within organizations and teams while providing an extra layer of confidence in external dependencies.

### Secure
The tools you build will be used by developers and security engineers alike, to help their teams to detect and mitigate potential vulnerabilities before they hit production.
This involves displaying results of our automated security scanning, for example Dependency Scanning.
The UI we build display aggregated data of scanning results in a user-friendly way.

### Release
Focus on building functionality related to the Continuous Delivery stage of the DevOps lifecycle. This includes contributing to Review Apps, Feature Flags, Merge Request Pipelines, and GitLab Pages. Our mission is to provide the best experience to developers as they deliver their changes to production with zero-touch software delivery.

### Configure & Serverless
Become an expert in Auto DevOps, Kubernetes and Serverless. You should also expect to create fantastic
user experiences that help guide users through configuring their application and infrastructure in an intuitive way.

### Monitor
Build charts and dashboards to help users monitor metrics so that users know how changes in their code impacts their production environment.
You should expect to become a leader in charting and data visualizations at GitLab and to become very familiar with our charting library, ECharts.

### Defend
Defend is a new group which will be bootstrapped during 2019.
Secure helps to improve security during the development of an application,
while Defend does so during the production phase of an application.
You should expect that there are a lot of interfaces to be built from scratch in Vue and VueX, potentially with real time updates and charting capabilities.

### Geo
TBD

### Fulfillment
Move data-driven product decisions forward by working on telemetry initiatives, understanding how users interact with GitLab. Work at the core of how GitLab earns revenue, working on our licensing and transaction applications.

### Ecosystem
TBD

### Distribution
TBD

## Relevant links

- [Engineering Handbook](/handbook/engineering)
- [Engineering Workflow](/handbook/engineering/workflow)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Selected candidates will be invited to complete a short written assessment.
* Next, candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a 60 minute technical and behavioral interview with a Frontend Hiring Manager
* Candidates will then be invited to schedule a 60 minute interview with the Director of Engineering
In some cases, we may ask the candidate to complete a technical take home challenge as an additional data point for measuring a candidate's technical aptitude
* Successful candidates will subsequently be made an offer.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
