---
layout: markdown_page
title: "Category Strategy - DevOps Score" 
---

- TOC
{:toc}

## Manage Stage

| Stage| Maturity |
| --- | --- |
| [Manage](/direction/manage/) | [Minimal](/direction/maturity/) |

### Introduction and how you can help
Thank you for visiting the category strategy page for DevOps Score. This page belongs to [Virjinia Alexieva](https://gitlab.com/valexieva) ([E-Mail](mailto:valexieva@gitlab.com), [Twitter](@virjinialexieva)).

This strategy is a work in progress and everyone can contribute by sharing their feedback directly on [GitLab.com](https://gitlab.com/groups/gitlab-org/-/epics/719), via e-mail, or Twitter.
<!-- Eric, I am going to use this epic and modify rather than start a new one -->

### Overview
<!-- A good description of what your category is today or in the near term. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Provide enough context that someone unfamiliar
with the details of the category can understand what is being discussed. -->
With [VSM](https://about.gitlab.com/direction/manage/value_stream_management/) and [Code Analytics](https://about.gitlab.com/direction/manage/code-analytics), GitLab is trying to improve organizations' understanding of how their people and codebase interact in order to improve the speed and quality of software delivery. More and more new users are signing up on GitLab.com and through that expanding community, we hope to be able to draw insights into how the usage of GitLab's different components correlates with and improves the efficiency, velocity and quality of an organization. We would also like to enable Executives to compare their organization with the rest of the community and identify areas for improvement. 

Our first attempt at providing instances with a single metric for GitLab adoption and engagement was [ConvDev Index](https://docs.gitlab.com/ee/user/instance_statistics/convdev.html). As soon as the index was released, however, we recognized that it has opened an enormous space for research, which will evolve with our software.

### Where we are Headed
<!-- Describe the future state for your category. What problems will you solve?
What will the category look like once you've achieved your strategy? Use narrative
techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is fully realized -->
We will start our journey by conducting research into how the usage of the different stages of GitLab correlates with positive results and attempt to quantify the relationships in a systematic, automated fashion. In the future, we hope we will be able to provide actionable recommendations to improve adoption and software development flows.

### Target Audience and Experience
<!-- An overview of the personas (https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas#user-personas) involved in this category. An overview 
of the evolving use cases and user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels. -->
We believe once developed, the score and related metrics would be of great interest to many Engineering and DevOps Managers, who are seeking to understand the adoption of the tool in more detail and how to better utilize it.

### What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->
[Exploring a new DevOps Score](https://gitlab.com/gitlab-org/gitlab-ce/issues/41780)

<!-- 
### What is Not Planned Right Now
Often it's just as important to talk about what you're not doing as it is to 
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand
the reasoning-->

### Maturity Plan
This category is currently at the `Minimal` maturity level and is planned to stay so for the rest of 2019. Please see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend) and related [epics](hhttps://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=group%3A%3Ameasure&label_name[]=devops%3A%3Amanage&label_name[]=Category%3A%3ADevOps%20Score).

<!-- It's important your users know where you're headed next. The maturity plan
section captures this by showing what's required to achieve the next level. The
section should follow this format:

Epic for a [Minimal Maturity](https://gitlab.com/groups/gitlab-org/-/epics/1479)
Epic for a [Viable Maturity](https://gitlab.com/groups/gitlab-org/-/epics/1480)

This category is currently at the XXXX maturity level, and our next maturity target is YYYY (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)). 

- Link to maturity epic if you are using one, otherwise list issues with maturity::YYYY labels) -->

<!-- 
### Competitive Landscape
The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

<!-- 
### Analyst Landscape
What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

<!-- 
### Top Customer Success/Sales issue(s)
These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

<!--
### Top user issue(s)
This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

<!--
### Top internal customer issue(s)
These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

<!-- 
### Top Strategy Item(s)
What's the most important thing to move your strategy forward?-->
