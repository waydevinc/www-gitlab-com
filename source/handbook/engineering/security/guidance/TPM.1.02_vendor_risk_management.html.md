---
layout: markdown_page
title: "TPM.1.02 - Vendor Risk Management Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# TPM.1.02 - Vendor Risk Management

## Control Statement

GitLab performs a risk assessment to determine the data types that can be shared with a managed service provider.

## Context

The purpose of this control is for GitLab to be very intentional about the data shared with any third parties. Every time we share GitLab data (including customer data) with a third party we increase the attack surface of that data. Since we rely on a number of third party services, we will need to share certain data; performing the risk assessment referenced in this control ensures that we are following a formal process of evaluating the information security program of any third parties and only sharing appropriate data when there is a legitimate need.

## Scope

This control applies to all information shared with third parties that interact with the GitLab production environment.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/TPM.1.02_vendor_risk_management.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/TPM.1.02_vendor_risk_management.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/TPM.1.02_vendor_risk_management.md).

## Framework Mapping

* ISO
  * A.13.2.2
  * A.15.1.1
  * A.15.1.2
  * A.15.1.3
  * A.15.2.2
* SOC2 CC
  * CC9.2
* PCI
  * 12.8
  * 12.8.2
  * 12.8.3
  * 12.8.5
  * 2.6
