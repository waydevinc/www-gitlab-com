---
layout: markdown_page
title: "BC.1.04 - Business Impact Analysis Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# BC.1.04 - Business Impact Analysis

## Control Statement

GitLab identifies the business impact of relevant threats to assets, infrastructure, and resources that support critical business functions. Recovery objectives are established for critical business functions.

## Context

* Business impact analysis (BIA) is a component of both business continuity planning but also of risk management.
* In the context of business continuity, BIAs help establish a priority for teams and services.
* Additionally, BIAs help document the risks associated with those teams and functions.
* BIAs help document the role of a team or service within the organization.
* BIAs are not meant to be comprehensive or perfectly reflect the impact that the team or service has on GitLab; BIAs are simply a way to threats and the related impact on business operations.

## Scope

BIAs should exist for all services and teams that have a business continuity plan.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/BC.1.04_business_impact_analysis.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/BC.1.04_business_impact_analysis.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/BC.1.04_business_impact_analysis.md).

## Framework Mapping

* ISO
  * A.17.1.1
  * A.17.1.2
* SOC2 CC
  * CC7.5
