---
layout: markdown_page
title: "Promotions and Transfers"
---

## On this page
{:.no_toc}

- TOC
{:toc}


## Introduction

At GitLab, we encourage team members to take control of their own career advancement. For all title changes, the team member should first speak with their manager to discuss a personal [professional development plan](/handbook/people-operations/learning-and-development/#career-mapping-and-development). If you feel your title is not aligned with your skill level, come prepared with an explanation of how you believe you meet the proposed level and can satisfy the business need.

As a manager, please follow the following processes, discuss any proposed changes with either the People Ops Business Partner or Chief People Officer, and do not make promises to the team member before the approvals are completed.

We recorded a training on this subject:

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/TNPLiYePJZ8" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Offer Process in BambooHR or Greenhouse

* Promotions for Individual Contributors in the same job family: reviewed and approved through the [BambooHR Promotion Process](/handbook/people-operations/promotions-transfers/#promotions--compensation-changes)
* Promotions or Applications to Manager level roles: All managers will need to apply for the open position in [Greenhouse](/handbook/hiring/offers/). They will go through the interview and approval process in Greenhouse to ensure anyone who would want to apply has the opportunity.
* Lateral Transfers to a different job family: apply and approval through the Greenhouse [hiring process](/handbook/hiring/).
* Promotions to Director and above: approval through the [BambooHR Promotion Process](/handbook/people-operations/promotions-transfers/#promotions--compensation-changes) to ensure the CFO/CEO have insight and approval into leadership promotions.

## Promotions & Compensation Changes

Any changes to title and compensation should be pushed through as quickly as possible. To promote or change compensation for one of your direct reports, a manager should follow the following steps:

- Create a Google Doc
  * Promotions: The manager (or team member) will create a path to promotion google doc which outlines the criteria of the role.  (i.e. promote based on performance, not based on potential). If the criteria for promotion is not adequately described on the relevant vacancy description page, then work on that first.
    * Try to remove feelings and use evidence; links to MRs, issues, or project work that clearly demonstrates the skills, knowledge and abilities of the person. There will be situations when an employee is ready to move to the next level through a promotion, however due to the nature of the business that particular role or next level may not be available for a business reasons.  Example the employee is ready for a Manager or Director role, however the business does not have the need, budget or scope for an additional manager/director at that time. The position may or may not become available in the future but it is not a guarantee.
    * [Values](/handbook/values/) alignment is also very important when considering promotions. This is especially the case for anyone who is moving to Senior. As one moves into management, the importance compounds.  Values thrive at a company only when we take them into consideration for hiring and promotions.
    * For promotions into a People Management role, you sure ensure the team member is prepared for management. This includes, at a minimum,  the ability and willingness to provide honest and direct performance feedback and coaching, interviewing candidates with an ability to be decisive, ability to have difficult conversations, and the ability to set direction, inspire, and motivate the team.
  * Compensation Only: Create a google doc outlining the reasons for the compensation increase also tying the rationale to our values and the vacancy description. Do not add compensation values to the google doc - only reasoning. Compensation values go into Bamboo only. Make the google doc accessible to anyone at GitLab who has the link.
- Complete an Experience Factor Worksheet
  * The worksheet should be completed by the manager in line with the [experience factor guidelines](/handbook/people-operations/global-compensation/#experience-factor-guidelines) by reviewing the team member with the new leveling criteria. For example, if the team member is promoted from intermediate to senior, a senior experience factor worksheet would be used to influence compensation for the promotion. This also helps to ensure that the team member is fully aware of any new expectations that come with the promotion.
- If the vacancy is being advertised via the [jobs page](/jobs/) the individual must submit an application for the role in order to be compliant with global anti-discrimination laws. Similarly, if there is no vacancy posting, one must be created and shared on the company call so that everyone has the opportunity to apply and be considered.
- Approval Process
  * If the position is a Director (or above) the promotion doc needs to be reviewed by the [e-group](/handbook/leadership/#e-group) in their weekly meeting **3 months before the promotion is intended to take place** and feedback may be provided to the individual and their manager to work on. This is to ensure consistent leadership qualities across the company. Exceptions to this timeline can be made at the e-group's discretion. Anyone who is mid-process as this rule goes into effect (March 2019) does not need to have 3 months added to their timeline but may receive the feedback, if any. We do not yet have a similar method to ensure consistency in external hires for Director (and above) roles. But we hope to add it soon.
  * The Direct Manager will submit the request in BambooHR
    1. Login to BambooHR.
    1. Select the team member you would like to adjust.
    1. In the top right hand corner, click Request a Change.
    1. Select which type of change you are requesting. If you are only looking to adjust compensation, then select Compensation. If this is a promotion that includes a title change and a compensation change, select Promotion. To only change the title, select Job Information.
    1. Enter in all applicable fields in the form, and then submit.
      * Note: Salary information is entered per payroll. For example, if the team member is in the United States, the annual salary is divided by 24 to get the pay rate to be entered. If the employee is in the Netherlands, please divide by 12.96. The divisor can be found in the "Pay Frequency" table in BambooHR. For any questions on how to fill out the form, please reach out to People Ops.
      1. In the comments section please include a link to a google doc and experience factor worksheet for all requests (promotion, compensation only, etc.) outlining the reasons for the proposed change. Please do not include salary information in the doc so it can be shared on the company call, if applicable.
      1. If there is a change to variable compensation, please fill out the "OTE Change" request separately.
      2. Managers should not communicate any promotion or salary adjustment until the request has gone through the entire approval process and you receive an adjustment letter from People Operations.
  * The request is next reviewed by the indirect manager in BambooHR.
  * Once the indirect manager approves the request, the Compensation & Benefits Manager will ensure the proposal adheres to the Global Compensation Calculator, with a comment in BambooHR outlining old compensation, new compensation, and a link to the salary calculator. People Operations will also comment on the number of additional [stock options](/handbook/stock-options/#stock-option-grant-levels) that the team member will be granted.
  * After people ops enters the old and new compensation the HR Business partner will review the business case for promotion, experience factor worksheet and proposed compensation change with the functional Group Leader.  The HR Business Partner will add to the comments that this process has been completed and that the Functional Group Leader approves the promotion.
  * Next, the CFO will review the request. If there are any questions, the CFO will add a comment outlining the specific concerns and the People Operations Analyst will ensure follow-up within a week to escalate to the CEO or deny the request.
  * The CEO reviews the request for final approval.
- If the request is approved, the People Operations Analyst will process the change and notify the manager.
- Promotions are then announced by the manager on the company call; where the manager describes how the individual met the promotion criteria and includes a link to the merge request where the individual's title is updated on the team page.
- When announcing or discussing a promotion on Slack, please include a link the promotion Google Doc to increase visibility for the reasons behind the promotion.
- If some amount of onboarding in the new role or offboarding from the old role is required (for example a change in access levels to infrastructure systems; switch in groups and email aliases, etc.), the manager will make an associated issue on the [GitLab Organization issue tracker](https://gitlab.com/gitlab-com/organization/) to list and track progress on those topics.

### Processing Promotion or Transfer Changes

If the request is approved through BambooHR the People Operations Analyst will create the Letter of Adjustment whereas if the request is through Greenhouse the People Operations Analyst will be notified via the People Ops email inbox that the letter has been signed. If this is the case, only data systems will need to be updated.

1. If approved in BambooHR, generate a [Letter of Adjustment](/handbook/contracts/#letter-of-adjustment). Enter all applicable information based on the BambooHR request. If there are no additional stock options, remove the section of the letter. The effective date is the next available payroll date or date of approval.
1. Approve the request, then update the entries in BambooHR to ensure there are the proper dates, amounts, and job information. Also, ensure to add stock options to the benefits tab if applicable. If the team member is moving to a Manager position, update their access level in BambooHR.
1. Ping the Compensation and Benefits Manager or the HR Business Partner in the `peopleops-confidential` channel in Slack to audit the letter.
1. Notify Payroll of the changes. This can be done in the following google docs: United States: "Payroll Changes", Everyone else: "Monthly payroll changes for non-US international team members". Payroll does not need to be notified for Contractors.
1. If the team member is a US Employee, also update the title and salary in Lumity.
1. Update the compensation calculator backend spreadsheet.
1. If the team member is in Sales or transferred to Sales, update the changes tab on the "Final Sales OTE FY 2020" google doc.
1. Once the letter has been audited, send an email to the manager confirming the approval. The Manager should never communicate the promotion or new salary until they have received this confirmation from People Ops. Once confirmed, the manager informs the individual of the promotion and changes (if any) in compensation.
  * Email template: `Your request for {team member}'s promotion has been approved! Please notify {team member}, announce on the company call linking to the reasons for promotion doc in view only mode ensuring all compensation information has been scrubbed, and reply all here for the Compensation team to stage the letter of adjustment in HelloSign. Please let me know if you have any questions!`
1. If the People Operations Analyst generated the letter of adjustment: once the manager confirms the team member has been notified, stage the letter in HelloSign for the Compensation and Benefits manager to sign.
1. Once signed by both parties, file in the Contracts and Changes folder in BambooHR.
1. If there was a title change associated with the request, People Ops will complete a merge request to change the title on the Team Page.

## Demotions

To demote one of your direct reports, a manager should follow the following steps:

- The manager should discuss any performance issues or possible demotions with the People Ops Business Partner in their scheduled meetings with a corresponding google doc.
- To initiate the process, the manager must obtain agreement from two levels of management.
- Proposed changes to a current vacancy description or a new vacancy description should be delivered with request for approval by the second level manager and the People Ops Generalist.
- Demotions should also include a review of [compensation](/handbook/people-operations/global-compensation) and [stock options](/handbook/stock-options/#stock-option-grant-levels) in the google doc. Managers should consult with People Operations on these topics; and of course always adhere to the Global Compensation Calculator.
- Once agreement is reached on the demotion and changes (if any) in compensation, send the google doc to the CEO for final approval.
- Once approved, the manager informs the individual. Please cc People Ops once the individual has been informed, to processes the changes in the relevant administrative systems, and stage a [Letter of Adjustment](/handbook/contracts/#letter-of-adjustment).
- Changes in title are announced on the company call.
- The manager will initiate any necessary onboarding or offboarding.

## Department Transfers

If you are interested in a vacancy, regardless of level, outside your department or general career progression, you can apply for a transfer.
You will never be denied an opportunity because of your value in your current role.

- If you are interested in a transfer, simply submit an application for the new position. If you are not sure the new role is a good fit, schedule time with the hiring manager to learn more information about the role and the skills needed. If after that conversation you are interested in pursuing the internal opportunity, it is recommended that you inform your current manager of your intent to interview for a new role. While you do not need your their permission to apply to the new role, we encourage you to be transparent with them. Most will appreciate that transparency since it's generally better than learning about your move from someone reaching out to them as a reference check. You can also use this as an opportunity to discuss the feedback that would be given to the potential new manager were they to seek it regarding your performance from your current and/or past managers. We understand that the desire to transfer may be related to various factors. If the factor is a desire NOT to work with your current manager, this can be a difficult conversation to have and shouldn't prevent you from pursuing a new role at GitLab.
- Transfers must go through the application process for the new position by applying on the [jobs page](/jobs). The team member will go through the entire interview process outlined on the vacancy description. If you have any questions about the role or the process, please reach out to your functional group's Human Resources Business Partner: Julie Armendariz for Sales and Finance; Jessica Mitchell for Marketing, Product, and Alliances; Carol Tesky for Engineering. In all cases, the applicable HR Business Partner should be informed before a transfer is confirmed.
- In the case of transfers, it is expected and required that the gaining manager will check with internal references at GitLab, including previous and current managers.
- Before the offer is made the recruiter will confirm with the team member and the gaining manager that they have indeed reached out to the current manager.  They will discuss the new internal opportunity and that an offer will be made to the team member.
- People Ops will ensure that, if applicable, the position has been posted for at least three business days before an offer is made.
- [Compensation](/handbook/people-operations/global-compensation) and [stock options](/handbook/stock-options/#stock-option-grant-levels) may be reviewed during the hiring process to reflect the new level and position.
- If after interview, the manager and the GitLab team-member want to proceed with the transfer, internal references should be checked. While a manager cannot block a transfer, there is often good feedback that can help inform the decision. It is advised that the GitLab team-member talk to their manager to explain their preference for the new team and to understand the feedback that will be given to the new manager. It should also be noted, that performance requirements are not always equal across roles, so if a GitLab team-member struggles in one role, those weakness may not be as pronounced in the new role, and vice versa.  However, if there are systemic performance problems unrelated to the specific role or team, a transfer is not the right solution.
- A new [contract](/handbook/contracts/#employee-contractor-agreements) will be sent out following the hiring process.
- If the GitLab team-member is chosen for the new role, the managers should agree on an reasonable and speedy transfer plan. 2 weeks is a usually a reasonable period, but good judgment should be used on completing the transfer in a way that is the best interest of the company, and the impacted people and projects.
- If the team member is transferred, the new manager will announce on the company call and begin any additional onboarding or offboarding necessary.

### Internal Department Transfers

If you are interested in another position within your department and the manager is also your manager you must do the following;

- Present your proposition to your manager with a google doc.
- If the vacancy is advertised on the [jobs page](/jobs/), to be considered, you must submit an application. If there is no vacancy posting, one must be created and shared in the #new-vacancies channel so that everyone has the opportunity to apply and be considered.
- The manager will asses the function requirements; each level should be defined in the vacancy description.
- If approved, your manager will need to obtain approval from their manager, through the chain of command to the CEO.
- [Compensation](/handbook/people-operations/global-compensation) and [stock options](/handbook/stock-options/#stock-option-grant-levels) will be reevaluated to ensure it adheres to the compensation calculator. Don't send the proposal to the CEO until this part is included.
- If the team member is transferred, the manager will announce on the company call and begin any additional onboarding or offboarding necessary.

**When promotion is a consideration - Within Same Job Family**

If a team member sees a vacancy posted that is the next level up within their [job family](https://about.gitlab.com/handbook/hiring/#definitions) (for example an Intermediate Frontend Engineer sees a vacancy for an Senior Frontend Engineer), the team member should have a conversation with their manager about exploring that opportunity.  Once that discussion is completed the team member should follow the internal department transfers guidance above.

It is the manager’s responsibility to be honest with the team member about their performance as it relates their promotion readiness. If the manager agrees that the team member is ready, then they will be promoted to the next level. If they do not think the team member is ready for the promotion, they should walk through the experience factor worksheet and their career development document, as well as work on a promotion plan with the team member. The manager should be clear that the team member is not ready for the promotion at this time and what they need to work on. If the team member would still like to submit an application for the role after the conversation with their manager, they can apply and go through the same interview process as external candidates. The recruiter will confirm with the manager that the promotion readiness conversation has taken place before the internal interview process starts.

**For Internal Applicants - Different Job Family**

If the role is in a completely different job family (within their own division or in a completely different division, for example, if a Product Designer is interested in a Product Manager role), the team member must submit an application via the posting on GitLab’s [internal job board](https://app2.greenhouse.io/internal_job_board) on Greenhouse.

After the team member applies, the recruiter will reach out to the team member to connect regarding compensation for the role. In some cases, the compensation may be lower than the current one. Once the team member understands and agrees with the compensation for the new role, they can continue the interview process.

Internal and external candidates will have the same process with the same amount of interviews and when possible the same interviewers, with the exception of the full screening call (which will be instead a short conversation to discuss compensation, as mentioned above). However, if the internal applicant will be staying in the same division and the executive level interview is a part of the process, the executive may choose to skip their interview. All interview feedback and notes will be captured in the internal team member’s Greenhouse profile, which will be automatically hidden from the team member. After interviews are completed, internal “reference checks” will be completed with the applicant’s current manager by the new hiring manager.

It is recommended that team members inform their manager of their desire to move internally and their career aspirations. Your manager should not hear about your new opportunity from the new hiring manager; it should come from you prior to the new hiring manager checking in for references with the current manager.

If you are unsure of the role, set up a coffee chat with the hiring manager to introduce yourself. Express your interest in the role and your desire to learn more about the vacancy requirements and skills needed. If after that conversation you do not feel that you are qualified or comfortable making the move, ask the hiring manager to provide guidance on what you can do to develop yourself so you will be ready for the next opportunity. It may also be possible to set up an [interning for learning](https://about.gitlab.com/handbook/people-operations/promotions-transfers/#interning-for-learning) situation with the hiring manager.

**For People Ops & Recruiting Team**

Vacancies will be posted both [externally and internally](https://about.gitlab.com/handbook/hiring/vacancies/#vacancy-creation-process) using Greenhouse for at least 3 business days.

Once an internal candidate completes their interviews and the internal references are completed, an offer needs to be submitted and approved in Greenhouse by working with the new manager, the People Business Partner, and the People Ops Analyst. Once the offer is approved, the offer will be extended verbally to the internal candidate by the hiring manager for the role.

After the offer is approved and extended by the hiring manager, the Recruiter will follow up with an email containing the details, and the Candidate Experience Specialist will prepare and send a Letter of Adjustment (LOA) through Greenhouse for the GitLab Signatory and the internal candidate to sign.

Once the contract is signed, the Candidate Experience Specialist will move the internal candidate to “Hired” in Greenhouse, making sure to **not** export to BambooHR.

After the internal candidate is moved to “Hired”, the recruiting team will notify the People Operations Analyst who will [update BambooHR]((/handbook/people-operations/promotions-transfers/#processing-promotion-or-transfer-changes) with appropriate changes.

### Leveling Up Your Skills

There are a number of different ways to enhance or add to your skill-set at GitLab, for example, if you do not feel like you meet the requirements for an inter-department transfer, discuss with your manager about allocating time to achieve this. This can be at any level. If you're learning to program, but aren't sure how to make the leap to becoming a developer, you could contribute to an open-source project like GitLab in your own time.

### Interning for Learning

If your manager has coverage, you can spend a percentage of your time working ('interning') with another team.

This could be for any reason: maybe you want to broaden your skills or maybe you've done that work before and find it interesting.

If your team has someone working part-time on it, it's on the manager of that team to ensure that the person has the support and training they need, so they don't get stuck. Maybe that's a buddy system, or maybe it's just encouragement to use existing Slack channels - whatever works for the individuals involved.

#### How does this work?

**What percentage of time should be allocated?** Well, 10% time and 20% time are reasonably common. As long as you and your manager have the capacity the decision is theirs and yours.

**What about the team losing a person for X% of the time? How are they supposed to get work done?**
Each manager needs to manage the capacity of their team appropriately. If all of the team are 'at work' (no one is on PTO, or parental leave, or off sick), and the team still can't afford X% of one person's time - that team might be over capacity.

**Can I join a team where I have no experience or skills in that area?**
That's up to the managers involved. It may be that the first step is to spend some time without producing anything in particular - in which case, it's possible that the [tuition reimbursement policy](/handbook/people-operations/code-of-conduct/#tuition-reimbursement) may be a better fit (Or it might not.)

**This sounds great but how do I initiate this process?**
First step is to discuss this with your manager at your next 1:1. Come prepared with your proposal highlighting what skills you want to learn/enhance and the amount of time you think you will need. Remember, this should be of benefit to you and GitLab. You and your manager will need to collaborate on how you both can make this happen which may also involve discussing it further with the manager of the team you may be looking to transfer to. All discussions will be done transparently with you. Be mindful though that the business needs may mean a move can't happen immediately.

**How do I find a mentor?**
On the [team page](/company/team), you can see who is willing to be a mentor by looking at the associated [expertise](/company/team/structure/#expert) on their entry.

#### Starting your new interning role

It's recommended that you work with your  current manager, the manager of the team you are interning with and your internship mentor to document
the percent of time, length of the commitment and goals for your internship. Ensure you reference the
[job responsibilities](https://about.gitlab.com/job-families/) of the role you are interning for when determining goals. Once finalized,
make sure that you enable public (to Gitlab) access to the document. Here is [an example](https://docs.google.com/document/d/1Oo7gnsFN5t_tQWgkYUqx3TWVRjlIiFvcG0veTabPH4g/edit#heading=h.vp8zbds2lyqa).

Once you've agreed upon the internship goals, both managers should inform their respective groups' People Ops HR Business Partner. On the start of
the internship you should use the public goals document to:
- Communicate the interning team members new role in their current team meeting and/or slack channel
- Communicate the interning team members new role in the team they are interning on's meeting and/or slack channel
- Communicate the interning team members new role on the company call as a "reintroduction"
- Update the interning team member's `team.yaml` entry to reflect the addition of the new role
