---
layout: markdown_page
title: "People Operations"
---

## Need Help?
{: #reach-peopleops}

Welcome to the People Operations handbook! You should be able to find answers to most of your questions here. You can also check out [pages related to People Operations](/handbook/people-operations/#other-pages-related-to-people-operations) in the next section below. If you can't find what you're looking for please do the following:

- [**The People Ops Group**](https://gitlab.com/gitlab-com/people-ops) holds several subprojects to organize people operations; please create an issue in the appropriate subproject or `general` if you're not sure. Please use confidential issues for topics that should only be visible to GitLab team-members. Similarly, if your question can be shared please use a public issue. Tag `@gl-peopleops` or `@gl-hiring` so the appropriate team member can follow up.
  * Please note that not all People Ops projects can be shared in an issue due to confidentiality. When we cannot be completely transparent, we will share in the issue description what we can share and why.
  * [**Employment Issue Tracker**](https://gitlab.com/gitlab-com/people-ops/employment/issues): only Onboarding, Offboarding, and Interview Training Issues are held in this subproject and are created by People Ops.
- [**Chat channel**](https://gitlab.slack.com/archives/peopleops); please use the `#peopleops` chat channel for questions that don't seem appropriate for the issue tracker. For access request regarding Google and Slack groups, kindly create an issue here: https://gitlab.com/gitlab-com/access-requests. For questions that relate to ADP, Payroll, Invoicing or Carta, kindly direct your questions to the `#finance` channel. With questions for our recruiting team, including questions relating to access or anything to do with Greenhouse, referrals, interviewing and interview training please use the `#recruiting` channel. For more urgent general People Operations questions, please use the mention `@peoplegeneral` to get our attention faster.
- If you need to discuss something that is confidential/private, you can send an email to the People Operations group (see the "Email, Slack, and GitLab Groups and Aliases" Google doc for the alias).
- If you only feel comfortable speaking with one team member, you can ping an individual member of the People Operations team, as listed on our [Team page](/company/team/).
- If you need help with any technical items, for example, 2FA, please ask in `#it-ops` and mention `@it-ops-team`.

## Other pages related to People Operations

- [Benefits](/handbook/benefits/)
- [Code of Conduct](/handbook/people-operations/code-of-conduct/)
- [Promotions and Transfers](/handbook/people-operations/promotions-transfers/)
- [Global Compensation](/handbook/people-operations/global-compensation/)
- [Incentives](/handbook/incentives)
- [Hiring process](/handbook/hiring/)
- [Group Conversations](/handbook/people-operations/group-conversations)
- [Leadership](/handbook/leadership/)
- [Learning & Development](/handbook/people-operations/learning-and-development/index.html)
- [Onboarding](/handbook/general-onboarding/)
- [Offboarding](/handbook/offboarding/)
- [OKRs](/company/okrs/)
- [People Operations Vision](/handbook/people-operations/people-ops-vision)
- [360 Feedback](/handbook/people-operations/360-feedback/)
- [Guidance on Feedback](/handbook/people-operations/guidance-on-feedback)
- [Collaboration & Effective Listening](/handbook/people-operations/collaboration-and-effective-listening/)
- [Gender and Sexual-orientation Identity Definitions and FAQ](/handbook/people-operations/gender-pronouns/)
- [Travel](/handbook/travel/)
- [Underperformance](/handbook/underperformance)
- [Visas](/handbook/people-operations/visas)

## On this page
{:.no_toc}

- TOC
{:toc}


## Role of People Operations
{: #role-peopleops}

In general, the People Operations team and processes are here as a service to the rest of the team; helping make your life easier so that you can focus on your work and contributions to GitLab. On that note, please don't hesitate to [reach out](/handbook/people-operations/#reach-peopleops) with questions! In the case of a conflict between the company and a team member, People Operations works "on behalf of" the company.

## Team Directory
{: #directory}

GitLab uses Slack profiles as an internal team directory, where team members can add their personal contact details, such as email, phone numbers, or addresses. This is your one-stop directory for phone numbers and addresses (in case you want to send your team mate an awesome card!). Feel free to add your information to your Slack profile (this is completely opt-in!) by clicking on "GitLab" at the top left corner of Slack, "Profile & Account", then "Add Profile" (for the first time making changes) or "Edit Profile" (if your account is already set up) to make any changes!

- Please make sure that your address and phone information are written in such a way that your team mates can reach you from a different country. So, for example, include `+[country code]` in front of your phone number.

## Anniversary Swag

Once per month, GitLab team-members with Anniversaries will be notified that they can redeem their Anniversary Gift in the Swag Store. Celebrate by sending the team member an email (template below) and post on the `#celebrations` channel on Slack so all team members can help them celebrate.

Anniversary Swag Template: "Happy GitLab Anniversary! Please use this code [insert code] to redeem your anniversary swag! You will need to login or create an account in the swag store to receive your special gift! The username will be your GitLab email. Please let People Ops know if you have any questions."

The code for the swag store anniversary campaign is located in the People Ops vault in 1Password.

Monitor the swag store open orders to place orders that team members have entered for their anniversary swag.

## Birthdays

The company encourages all GitLab team-members to take a day of vacation on their birthday by utilizing our [paid time off](/handbook/paid-time-off/) policy.

## Letter of Employment and Reference Request

If you need a letter from GitLab verifying your employment/contractor status, please send the request to People Ops via email at [peopleops@gitlab.com](mailto:peopleops@gitlab.com) and cite what information is needed. We will provide most recent title, dates of employment, and salary information. We will also verify, but not provide National Identification Numbers. People Ops will send you the letter once it is completed. In addition, if the request comes from a third party, People Ops will always verify that the information is appropriate to share.

GitLab team-members are not authorized by the company to speak on its behalf to complete reference requests for GitLab team-members no longer working for GitLab. If a team member would like to give a personal reference based on their experience with the former team member, it must be preceded by a statement that the reference is not speaking on behalf of the company. To reinforce this fact, personal references should never be on company letterhead and telephone references should never be on company time. You do not need permission from GitLab to give a personal reference. Remember to always be truthful in reference check and try not to give a majority negative reference; instead refuse to provide one. Negative references can result in legal action in some jurisdictions.

If an ex team member acted in a malicious way against GitLab we'll do a company wide announcement on the company call not to provide a reference.

## Boardroom addresses
{: #addresses}

- For the SF boardroom, see our [visiting](/company/visiting/) page.
- For the NL office, we use a postbox address listed in the "GitLab BV address" note in the Shared vault on 1Password. We use [addpost](https://www.addpost.nl) to scan our mail and send it along to a physical address upon request. The scans are sent via email to the email alias listed in the "Email, Slack, and GitLab Groups and Aliases" Google doc.
- For the UK office, there is a Ltd registered address located in the "GitLab Ltd (UK) Address" note in the Shared vault on 1Password
- For the Germany office, there is a GmbH address located in the "GitLab GmbH Address" note in the Shared vault on 1Password

## Business Cards

Business cards can be ordered through Moo.  Please let People Operations know if you have not received access to your account on Moo by the end of your first week with GitLab.  Place the order using your own payment and add it to your expense report.  If you need any assistance, let People Ops know.

Once you are logged in, you will follow these steps to create your cards:

1. Select the "+" sign in the upper right-hand corner of your screen.
1. Select your currency in the upper right corner to ensure that your shipment is sent from the correct location.
1. Select "Business Cards".
1. Select your template (one has the Twitter & Tanuki symbol and cannot be removed, and one is free of those symbols).
1. Enter your information into the card.
1. Please remember to choose rounded corners.
1. Add the card to your cart and order! We ask for a default of 50 cards unless you are meeting with customers regularly.
1. Add the cards to your expense report under 'office supplies'

### Business Cards - India

Since MOO ships business cards from outside India via DHL, DHL is mandatorily required to perform "Know Your Customer" (KYC) checks before delivery.
If you are a team member residing in India, please consider using the following tips while ordering business cards from MOO.

- Avoid filling the "Company name" field while checking out your order to prevent the shipment being sent in the company's name instead of your name. It is necessary to fill only the "First Name" and "Last Name" fields.
- For verification, DHL matches the name and address in your ID proof with the name and address in your consignment. So, while providing the address for delivery, make sure to provide the same address as in one of your ID proofs.
- In a scenario where you do not have any ID proof associated with the address you intend to provide, consider shipping the item in the name of somebody who holds a valid ID proof in that address (eg: a relative or a friend)
- Please check out the list of [valid KYC documents](https://dhlindia-kyc.com/forms/valid-kyc-docs.aspx#ind-indian) before placing the order.


## NPS Surveys

NPS stands for "Net Promoter Score". GitLab has two forms of NPS surveys: eNPS for all employees (where "e" stands for "Employee"), and an onboarding NPS. These surveys gauge employee satisfaction and how likely employees are to recommend GitLab to others as a place to work.

People Ops will send out an eNPS survey twice yearly to all employees.

The onboarding NPS survey is a 60-day survey for new hires. To help People Ops understand your experience and improve the onboarding process, please complete the [onboarding survey](https://docs.google.com/a/gitlab.com/forms/d/e/1FAIpQLSdU8qxThxRu3tdrExFGBJ1GlyucCoJj2kDpcvOlM4AiRGOavQ/viewform) after you have been working for GitLab for at least 2 months. People Ops will send reminders to team members to complete the survey.

## Gifts

People Operations will send flowers for a birth, death, or significant event of a team member. This policy applies to an immediate relationship to the GitLab team-member. Management can send a request to People Operations in order to ensure that a gift is sent in the amount of [75-125 USD](/handbook/people-operations/global-compensation/#exchange-rates).

## Using BambooHR

We use [BambooHR](https://gitlab.bamboohr.com) to keep all team member information
in one place. All team members (all contract types) are in BambooHR.
We don't have one contact person but can call BambooHR if we want any changes made in the platform. The contact info lives in the Secretarial Vault in 1Password.

Some changes or additions we make to BambooHR require action from our team members.
Before calling the whole team to action, prepare a communication to the team that is approved by the Chief People Officer.

Team Members have employee access to their profile in BambooHR and should update any data that is out-dated or incorrect. If there is a field that cannot be updated, please reach out to the People Ops Analyst with the change.

## Using RingCentral

Our company and office phone lines are handled via RingCentral. The login credentials
are in the Secretarial vault on 1Password. To add a number to the call handling & forwarding
rules:

- From the Admin Portal, click on the Users button (menu on left), select the user for which you
want to make changes (for door handling, pick extension 101).
- A menu appears to the right of the selected user; pick "Call Handling & Forwarding" and review
the current settings which show all the people and numbers that are alerted when the listed User's
number is dialed.
- Add the new forwarding number (along with a name for the number), and click Save.

## WBSO (R&D tax credit) in the Netherlands

For roles directly relating to Research and Development in the Netherlands, GitLab may be eligible for the [WBSO (R&D Tax Credit)](http://english.rvo.nl/subsidies-programmes/wbso).

### Organizing WBSO

**Applications**

As of 2019 GitLab must submit three applications each year and the deadlines for those are as follows:

1. **31 March 2019**, for period May - August 2019 (Product Manager for Create Features)
1. **31 August 2019**, for period September - December 2019 (Product Manager for Gitaly)
1. **30 November 2019**, for period January - April 2020 (Product Manager for Geo Features)

There is a [translated English version of the application template](https://docs.google.com/document/d/15B1VDL-N-FyLe84mPAMeJnSKjNouaTcNqXeKxfskskg/edit) located in the WBSO folder on the Google Drive. The applications should be completed by a Product Manager, responsible for features or a service within GitLab, who can detail the technical issues that a particular feature will solve. Assistance on completing the application can also be sought from the WBSO consultant (based in the Netherlands). The contact details for the consultant can be found in a secure note in the People Ops 1Password vault called WBSO Consultant. People Operations will assist with co-ordinating this process. It is currently owned by Finance.

**Hour Tracker**

Each year a spreadsheet with the project details and hours logged against the work done on the project(s) will need to be created. This is for the entire year. The current hour tracker is located in the WBSO folder, on the Google Drive, and shared only with the developers that need to log their hours (located in the Netherlands), People Operations, Finance and the WBSO consultant. Once the projects have been completed for the year, the WBSO consultant will submit the hours and project details to claim the R&D grant from the [RVO](https://english.rvo.nl/). The WBSO consultant will contact People Operations should they have any queries.

## Paperwork people may need to obtain mortgage in the Netherlands

When your employment contract is for a fixed period of time (e.g. 12 months) you'll need a "werkgeversverklaring".
This document describes your salary and states that your employer expects to continue to employ
you after the contract expires (assuming the performance of the employee doesn't degrade).
This document has to be filled in by hand, preferably using blue ink, and must be signed
and stamped. If there is no stamp (as is the case for GitLab) an extra letter (also signed)
must be supplied that states the employer has no stamp. While the language of these
documents doesn't matter, the use of Dutch is preferred.

Employees also have to provide a copy of a payslip that clearly states not only their
monthly salary but also their annual salary. These numbers must match the numbers on
the "werkgeversverklaring" down to the decimals. Mortgage providers may also require
you to provide information about your financial status, usually in the form of a report/screenshot
of your total financial status (including any savings you have). The requirements for
this seem to vary a bit between mortgage providers.
