---
layout: markdown_page
title: "CEO Shadow Program"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

The CEO shadow program at GitLab is not a job title but a temporary assignment to shadow the CEO.
The shadow(s) will be present at all meetings of the CEO. GitLab is [all remote](/company/culture/all-remote/) but the CEO has in-person meetings with external organizations. Therefore, you will live in San Francisco during the entire [rotation](#rotation-rhythm) and travel with the CEO.

## Goal

The goal of the shadow program is to give current and future [directors and senior leaders](/company/team/structure/) at GitLab an overview of all aspects of the [company](/company/).
This should lead to leadership who are able to do [global optimizations](/handbook/values/#global-optimization). The program will also create opportunities for the CEO to develop relationships with team members across the company to identify challenges and opportunities earlier.

## Reasons to participate

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/Cg0LzET_NWo" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

## What to expect

### Meeting attendance

You will attend all meetings of the CEO, this includes but not limited to:

1. 1-1s with reports
1. interviews with applicants
1. conversations with board members

You will travel with the CEO to meetings, team off-sites and conferences outside of San Francisco per the CEO's schedule. EA to the CEO will assist you with conference registration and travel accommodations during these time frames.

The CEO's executive assistant should ask external people if
they are comfortable with the shadow joining prior to the scheduled meeting and will share a link to the CEO Shadow page to provide context.

Meeting agendas should be shared with ceo-shadow@gitlab.com. For agendas that contain sensitive information, the sensitive information should be removed and the document shared with "View only" access to restrict access to the document's history. If an agenda has not been shared, the CEO Shadow should contact the document owner for access.

These meetings can have different formats:

1. video calls
1. in-person meetings
1. dinners that are business related
1. customer visits
1. conferences

You will not attend when:

1. Someone wants to discuss a complaint and wants to stay anonymous.
1. If any participant in the meeting is uncomfortable.
1. If the CEO wants more privacy.

This is probably the most open program in the world.
It depends on the participants respecting confidentiality, during the program, after the program, and after they leave GitLab.

### CEO shadow introductions

When introducing yourself, first state that you're a CEO shadow for two weeks then your job title.

### Rotation rhythm

We want many people to be able to benefit from this program, therefore we rotate often.
It is important that an incoming person is trained so that the management overhead can be light.
Currently, a rotation is two weeks:

1. See one, you are trained by the outgoing person.
1. Teach one, you train the incoming person.

The shadow should be available for the full two weeks.

When the CEO has a week or more of paid time off or during [Contribute](https://about.gitlab.com/events/gitlab-contribute/) the shadow program will pause, one shadow will "see one" before the break and "teach one" after the break. The rotations with breaks of one or more weeks without a shadow are great if you can't to be away from home for more than one week at a time.

Also, if you'll need childcare to be able to participate, GitLab will [reimburse you](https://about.gitlab.com/handbook/spending-company-money/) for it.

This program is not limited just to long-term GitLab team members. For new team members this might be the first thing they do after completing our [onboarding](/handbook/general-onboarding/). Exceptional community members may be able to participate also. 

### Other things to know

1.  This isn't a performance evaluation, so get comfortable.
1.  You do not need to dress formally, so business casual clothes are appropriate. For example, a button up with jeans. Bring comfortable shoes with you to Mission Control any time there are meetings in the city. High heels are not a good idea. Sid prefers to walk, even if his calendar says Uber. 
1.  Review the CEO's calendar to get an idea of what your upcoming weeks will be like.
1.  Review and update the ongoing CEO agenda. This agenda contains TODOs, documentation items, training, and feedback information items.
1.  Be ready to observe and ask questions.
1.  Give and receive feedback from the CEO.
1.  Don't plan to do any of your usual work. Prepare your team as if you were on vacation.
1.  Be ready to add a number of handbook updates during your shadow period. 

## Eligibility

You are eligible to apply for the program if you haved accepted an offer at GitLab as a:

1. [Director or up](/company/team/structure/#layers) or [Distinguished backend engineer or up](https://about.gitlab.com/job-families/engineering/backend-engineer/) since we have a dual career track for engineering.
1. [Manager](/company/team/structure/#layers) or [Staff engineer](https://about.gitlab.com/job-families/engineering/backend-engineer/) if there is 1 consideration.
1. [Individual Contributors](/company/team/structure/#layers) if there are 2 consideration.

Considerations are cumulative and can be:

1. You belong to an underrepresented group as defined in our [referral bonus program](/handbook/incentives/#referral-bonuses).
1. There is a last minute cancellation and you respond quickly.
1. Recipient of GitLab’s value award of transparency, collaboration, iteration, efficiency, results or diversity at the most recent Contribute.
1. You work(ed) at an investor and helped with an investment in GitLab in a private round :)

## How to apply

- Create a merge request to add yourself to the [rotation schedule](#rotation-schedule).
- Ask your manager to approve the merge request (don't merge it yet).
- Assign the merge request to the CEO, link it in the #ceo-shadow channel, and @mention the CEO and the EA supporting the CEO in the message.

## Rotation schedule

| Start date | End date | See one | Teach one |
| ------ | ------ | ------ | ------ |
| 2019-06-17 | 2019-06-21 | Clinton Sprauve - Technical Marketing Manager | Nnamdi Iregbulem - MBA Candidate at Stanford University |
| 2019-06-24 | 2019-06-28 | Lyle Kozloff - Support Engineer Manager | Clinton Sprauve - Technical Marketing Manager |
| 2019-07-01 | 2019-07-05 | No Shadow - Vacation | No Shadow - Vacation |
| 2019-07-08 | 2019-07-12 | Marin Jankovski - Engineering Manager, Delivery | Lyle Kozloff - Support Engineering Manager |
| 2019-07-15 | 2019-07-29 | No Shadow - Vacation | No Shadow - Vacation |
| 2019-08-05 | 2019-08-09 | Emilie Schario - Data Analyst, Finance | Marin Jankovski - Engineering Manager, Delivery |
| 2019-08-12 | 2019-08-16 | Kenny Johnston - Director of Product, Ops | Emilie Schario - Data Analyst, Finance  |
| 2019-08-19 | 2019-08-23 | JJ Cordz - Senior Marketing Operations Manager | Kenny Johnston - Director of Product, Ops |
| 2019-08-26 | 2019-09-06 | No Shadow - Vacation | No Shadow - Vacation |
| 2019-09-09 | 2019-09-13 | No Shadow - Conference | No Shadow - Conference |
| 2019-09-16 | 2019-09-20 | Eric Brinkman - Director of Product | JJ Cordz - Senior Marketing Operations Manager |
| 2019-09-23 | 2019-09-27 | Danielle Morrill - General Manager, Meltano | Eric Brinkman - Director of Product |
| 2019-09-30 | 2019-10-04 | No Shadow - Vacation | No Shadow - Vacation |
| 2019-10-07 | 2019-10-11 | Mek Stittri - Director of Quality | Danielle Morrill - General Manager, Meltano |
| 2019-10-14 | 2019-10-18 | No Shadow | No Shadow |
| 2019-10-21 | 2019-10-25 | Kyla Gradin - Mid Market Account Executive | Mek Stittri - Director of Quality |
| 2019-10-28 | 2019-11-01 | Clement Ho - Frontend Engineering Manager  | Kyla Gradin - Mid Market Account Executive |
| 2019-11-04 | 2019-11-08 | Chenje Katanda - Support Engineer  | Clement Ho - Frontend Engineering Manager |
| 2019-11-11 | 2019-11-15 | AVAILABLE  | Chenje Katanda - Support Engineer (2019-11-04 rotation) |
| 2019-11-18 | 2019-11-22 | AVAILABLE  | AVAILABLE (2019-11-11 rotation) |
| 2019-11-25 | 2019-12-31 | No Shadow | No Shadow |

If you have questions regarding the planned rotation schedule, please ping the EA to CEO. 

## What does a shadow do?

### Tasks

Since a rotation is over a short period there are no long running tasks that you can assume.

The tasks consist of short term tasks, for example:

1. Make [handbook](/handbook/) updates.
1. Iterate and complete small tasks as they come. Clear them out immediately to allow for rapid iteration on more crucial tasks.
1. Draft email responses.
1. Solve urgent issues, for example a complaint from a customer or coordinating the response to a technical issue.
1. Prepare meetings.
1. Take notes during meetings.
1. Follow up on meetings (improve the notes, followup appointment, thank you cards, swag).
1. Ensure that internal teams are updated with action items and outcomes.
1. Compile a report on a subject.
1. Write a blog post based on a conversation.
1. Editing a recorded video.
1. Promoting the work you created.
1. Writing a blog post about something you learned or your experience.
1. Camerawork for a video.
1. Control slides and muting during the board meeting.
1. Set up for an in-person meeting.
1. Receive guests.
1. Add to the documentation of GitLab.
1. Do and publish a [CEO interview](https://gitlab.com/gitlab-com/www-gitlab-com/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=CEO%20interview).
1. Add training for this program to the handbook.
1. Training the incoming shadow program person.
1. Speak up when the [CEO displays flawed behavior](https://about.gitlab.com/handbook/ceo/#flaws).

### Follow activity from the CEO in Slack
Shadows are encouraged to follow the CEO's activity on Slack to gather a complete picture of his everyday engagements.
Go to the Slack search bar and type "from:@sid" and it will populate the results.

![Slack User Activity](/images/ceoshadow/slackuseractivity.png){: .shadow.medium.center}
Follow Sid's Slack activity to follow his everyday engagements 
{: .note.text-center}

### Documentation focus

An ongoing shadow program with a fast rotation is much more time consuming for the CEO then a temporary program or a rotation of a year or longer.
Therefore most organizations either have a shadow for two days or have someone for a year or more.
We want to give many people the opportunity to be a shadow so we rotate quickly.
To make this happen without having to invest a lot of time to train people coming in we need great documentation.
Therefore a quick turnaround on documentation is of paramount importance.
And the documentation will have a level of detail that isn't needed in other parts of the organization.

### Attending events with the CEO

When attending events with the CEO, keep the following in mind:
1. Remind the CEO to bring extra business cards before leaving. And bring a few for yourself.
1. When traveling to events on foot, CEO Shadows should take responsibility for navigating to the event. 
1. After a talk or panel, be ready to help the CEO navigate the room, particularly if there is a time sensitive obligation after the event.

## Training checklist

Outgoing shadows are responsible for training incoming shadows. Here's a list of things to make sure you cover:

1. Boardroom location & WiFi.
1. Set a meeting time and place for the incoming Shadow's first day. The Outgoing Shadow will give the Incoming Shadow access to the boardroom.
1. At the start of the week, review your calendar and the CEO's. If a meeting wasn't added to your calendar and looks like it should be, reach out to CEO executive assistant to confirm if you should be invited or not. There will be some meetings and events the Shadow does not attend.
1. Discuss coordinating schedules and key access to avoid ringing the CEO.
1. How to set up the board room for in-person meetings (boardroom TV, answering the phone, greeting the guest).
1. How to update the boardroom TV screens.
1. When to show up to the boardroom, getting to events, dinners, etc.
1. How to set up a livestream. Make sure you are added as a co-host for any planned livestreams ahead of time.  
1. Ensure incoming Shadow has access to the CEO Shadow agenda and knows how to make changes to the handbook.

## Boardroom Guide

### Working from the boardroom

You are welcome to work from the boardroom but it is not required to be present in-person unless there is an in-person meeting, event, or dinner. It's up to you to manage your schedule and get to places on time. If you are traveling somewhere, meet the CEO at the boardroom at the beginning of the alloted travel time listed on the calendar.

If there is a day during your program where all meetings are Zoom meetings, you can work from wherever you want, as your normally would. You can work from the boardroom if you prefer. If you decide to split your day between remote work and working from the boardroom, make sure you give yourself enough time to get to the boardroom and set up for the guest. It's OK to join calls while mobile.

Shadows are welcome in the boardroom from 7:30 until 6pm. Feel free to ask if you can stay later. Don't worry about overstaying you're welcome, if Karen or Sid would like privacy they will ask you to leave explicitly.

One more thing: the cat feeder is automatic and goes off daily at 10:22am PT. No need to be alarmed by the metallic clanging sound.

### Boardroom access

When entering the building, the doorperson will ask who you are there to see. Don't say "GitLab" since there is no GitLab office. The doorperson will direct you to the correct lobby.

There is one set of keys so the shadows will need to coordinate access to the boardroom. The fob is for the elevator and the key is for the boardroom.

### Boardroom monitors

#### Configuring the monitors
{:.no_toc}

We have six monitors in the boardroom. They should be configured as follows:
- Top Right: [Team](/company/team/)
- Top Middle: Clari Sales Dashboard - This Quarter
- Top Left: [Stages from homepage](/)
- Bottom Right: [Category Maturity](/direction/maturity/)
- Bottom  Middle: Clari Sales Dashboard - Next Quarter
- Bottom Left: [Who we replace](/devops-tools/)

To configure the sales dashboards:

1. Go to [Clari](https://app.clari.com)
1. Go To Pulse tab
2. Open the left side bar
3. Click on the funnel icon. Select “CRO”
4. Click on the gear icon. Go to Forecasting. Select Net IACV.

#### How to use keyboard and mouse to update screens
{:.no_toc}

The wireless mouse and keyboard are connected to the bottom left TV by default because that one is visible from both sides of the conference table. To update the view on another TV, you have to connect the wireless keyboard and mouse to the desired screen. Afterwards don't forget to return it to the bottom left position for use during meetings.

1. Find the USB jacks and Logitech receiver underneath the bottom, right TV.
1. Connect the Logitech receiver to USB receiver for the desired screen.
1. To log into the chrome devices in the board room, you must be added to the "[CEO Shadow Vault](https://gitlab.1password.com/vaults/tq7hndbk2xjcwc5czwcq7qt6kq/allitems/piqiei3hcjdz7lpmpsyppwxmfi)" in 1Password. If you do not have persmission, please ask the CEO EA for access. 

#### AirPlay
{:.no_toc}
To screencast from an iPad or MacBook to the monitors, switch the "Source" on the bottom middle screen to "Apple TV" by navigating through the bottom menu. Click the home button on the remote control for that screen to open the menu. If the AppleTV is not recognized, use the remote control to turn the device on.

### Visitor prep

In preparation for guests (customers, investors, etc.) who will be meeting with the CEO or other team members at the Boardroom, please note the following prior to the meeting start:

1. All GitLab team-members sit on one side. This allows for easy communication to the guests
1. Have drinks available on table (from the fridge)
1. If someone will be attending a meeting in the boardroom via Zoom, you'll need to set up the TV on the rolling cart following these steps:
    1. Turn on the TV and the attached Apple TV unit
    1. Open the Zoom Meetings app on the iPad (located on the stand next to the Apple TV)
    1. Click 'Join' in  the Zoom Meetings menu and enter the number for the meeting

### Boardroom FAQs

1. Everything in the fridge that is liquid can be drank including Soylent and alcohol
1. Thermometer located next to Kitchen entrance
1. Lighting located next to Kitchen entrance

## Expenses, travel and lodging

### Lodging
Lodging during the CEO shadow program is provided by the company. CEO executive assistant books the accommodation based on availability and cost. You can express your preference (hotel or AirBnB) via email to the EA in question, however the final decision is made by the EA based on the distance from CEO and costs. EA will provide the accommodation details no earlier than 1 month and no later than 2 weeks before the scheduled rotation.

Accommodation is provided only for the active shadowing period, it is not provided during the shadow program pause (cases when the CEO is unavailable).
In case you are coming from a timezone that is more than 6 hours difference with Pacific Time, it is possible to book the weekend before the first shadow work day to adjust to the new timezone.

### Airfare
Airfare can be booked according to our [travel policy](/handbook/travel/#booking-travel-and-lodging) or [spending company money](https://about.gitlab.com/handbook/spending-company-money/) policy.
In case your shadow rotation includes time without shadowing, it is possible to expense airfare to fly home and back within the continental USA. If you are from outside of the USA, it is also possible to expense airfaire during the time without shadow because of the possible high cost of lodging in San Francisco if you chose to stay at a different location.

### Childcare
Childcare is provided during the active shadowing period and will be reimbursed via your expense report. You must book the childcare yourself and it is advised you reach out far in advance as childcare "drop-ins" can be limited depending on the week. Currently, GitLab doesn't have a ["Backup Care"](https://www.brighthorizons.com/family-solutions/back-up-care) program so you must tell the childcare it is for a "drop-in".  Depending on your hotel accommodations, finding a nearby daycare is most convenient or a daycare nearby the [Millennium tower](https://www.google.com/maps/place/Millennium+Tower+San+Francisco/@37.7905055,-122.3962516,15z/data=!4m2!3m1!1s0x0:0x9fe15ebd4a8300d8?sa=X&ved=2ahUKEwiUoZ_hpb_iAhXBop4KHeOAB2QQ_BIwGHoECAsQCA). Some childcare facilities will require payment at end-of-day or end-of-week via cash/check only so request an invoice/receipt for expense submission purposes. 

Past Childcare facilities that have been accommodating:

1. [Bright Horizons at 2nd Street](https://child-care-preschool.brighthorizons.com/ca/sanfrancisco/2ndstreet?utm_source=GMB_yext&utm_medium=GMBdirectory&utm_campaign=yext&IMS_SOURCE_SPECIFY=GMB) - This facility is nearest the [Courtyard by Marriott SF Downtown Hotel](https://www.google.com/maps/place/Courtyard+by+Marriott+San+Francisco+Downtown/@37.785751,-122.3997608,16.7z/data=!4m12!1m6!3m5!1s0x8085807c643d1007:0x85815e04bf8d233c!2sCourtyard+by+Marriott+San+Francisco+Downtown!8m2!3d37.7859011!4d-122.3969222!3m4!1s0x8085807c643d1007:0x85815e04bf8d233c!8m2!3d37.7859011!4d-122.3969222). 
    * Contact: [Rose - Current Director](https://child-care-preschool.brighthorizons.com/ca/sanfrancisco/2ndstreet)

## Naming

For now this role is called a [CEO shadow](https://feld.com/archives/2015/03/ceo-shadowing.html) to make it clear to external people why a shadow is in a meeting.

Other names considered are:

1. Technical assistant. Seems confusing with [executive assistant](/job-families/people-ops/executive-assistant/). ["In 2003, Mr. Bezos picked Mr. Jassy to be his technical assistant, a role that entailed shadowing the Amazon CEO in all of his weekly meetings and acting as a kind of chief of staff. "](https://www.theinformation.com/articles/amazons-cloud-king-inside-the-world-of-andy-jassy)
1. Chief of Staff. This commonly is the ["coordinator of the supporting staff"](https://en.wikipedia.org/wiki/Chief_of_staff) which is not the case for this role since people rotate out of it frequently. The executive assistants reports to peopleops.
1. [Global Leadership Shadow Program](https://www2.deloitte.com/ng/en/pages/careers/articles/leadership-shadow-program.html) is too long if only the CEO is shadowed.

## Cat

Please note that we have a cat named [Suzy](/company/team-pets/#7-suzy). It is a Russian Blue mix which is a [hypoallergenic variety](https://www.russianbluelove.com/russian-blue-cat-allergies/). If you're allergic to cats consider washing you hands after petting.

Suzy likes attention and will invite you to pet her. Please don't pet her after she meows since that reinforces the meowing which can be annoying during calls and the night. You can pick her up but she doesn't like it much and will jump out after about 30 seconds.

## Recommended Food/Drinks nearby

### Food
1. [Food Truck Stop](https://www.google.com/maps/place/Truck+Stop/@37.7903558,-122.3973943,19.6z/data=!4m12!1m6!3m5!1s0x0:0xfa5e2c445f996f86!2sSalesforce+Landmark!8m2!3d37.7941181!4d-122.3949838!3m4!1s0x8085806312a2e16d:0xd6687ec711cfdbf8!8m2!3d37.7901391!4d-122.3974545)
1. [La Fromagerie Cheese Shop](https://www.google.com/maps/place/La+Fromagerie+Cheese+Shop/@37.7893648,-122.3978068,21z/data=!4m12!1m6!3m5!1s0x0:0xfa5e2c445f996f86!2sSalesforce+Landmark!8m2!3d37.7941181!4d-122.3949838!3m4!1s0x808581e271b2d051:0x7207769e6f11a6a1!8m2!3d37.7893649!4d-122.3978294)
    * Mayank Tahil Favorite
1. [Sausilito Cafe](https://www.google.com/maps/place/Sausalito+Cafe/@37.7893313,-122.3978205,20.99z/data=!4m5!3m4!1s0x808580633e7e631f:0xa260917d36817274!8m2!3d37.7893929!4d-122.3978294)
    * Tye Davis / Mayank Tahil / John Coghlan Favorite
1. [Uno Dos Taco](https://www.google.com/maps/place/Uno+Dos+Tacos/@37.7891127,-122.4030043,17z/data=!4m12!1m6!3m5!1s0x808580e9b3089847:0x3a964d5d97defd44!2sUno+Dos+Tacos!8m2!3d37.7891127!4d-122.4008156!3m4!1s0x808580e9b3089847:0x3a964d5d97defd44!8m2!3d37.7891127!4d-122.4008156)
    * John Coghlan / Tye Davis / Mayank Tahil Favorite
1. [The Bird](https://www.google.com/maps/place/The+Bird/@37.788018,-122.3994844,18.43z/data=!4m12!1m6!3m5!1s0x8085808814a10285:0x3b8f6e4330a367d9!2sBluestone+Lane!8m2!3d37.7878896!4d-122.4028954!3m4!1s0x8085807d4a010713:0x758a9eee77a59f73!8m2!3d37.787243!4d-122.4000985)
1. [Joe & The Juice](https://www.google.com/maps/place/JOE+%26+THE+JUICE/@37.7893287,-122.3951635,19.45z/data=!4m5!3m4!1s0x80858064ca640bff:0x7f1d4922e3ad6a4a!8m2!3d37.7896722!4d-122.3942788)
1. [Cafe Venue](https://www.google.com/maps/place/Cafe+Venue/@37.7893287,-122.3951635,19.45z/data=!4m5!3m4!1s0x8085807b358d9911:0x6cf96332b268be9c!8m2!3d37.7891937!4d-122.3948957)
1. [Walgreens](https://www.google.com/maps/place/Walgreens/@37.7898933,-122.3978574,20.07z/data=!4m5!3m4!1s0x80858064424ba17f:0x1c27c67d47e4fe8b!8m2!3d37.7900166!4d-122.3975766)
1. [CEO's Favorite Restaurants](https://about.gitlab.com/handbook/ceo/#favorite-restaurants)

### Drink
1. [Bluestone Lane](https://www.google.com/maps/place/Bluestone+Lane/@37.7900021,-122.4031358,16.56z/data=!4m12!1m6!3m5!1s0x8085808814a10285:0x3b8f6e4330a367d9!2sBluestone+Lane!8m2!3d37.7878896!4d-122.4028954!3m4!1s0x8085808814a10285:0x3b8f6e4330a367d9!8m2!3d37.7878896!4d-122.4028954)
    * Tye Davis Favorite (AMAZING COFFEE) - long walk
1. [Starbucks](https://www.google.com/maps/place/Starbucks/@37.7899941,-122.3977364,18.43z/data=!4m12!1m6!3m5!1s0x8085808814a10285:0x3b8f6e4330a367d9!2sBluestone+Lane!8m2!3d37.7878896!4d-122.4028954!3m4!1s0x80858067244672f9:0xea9a5743328af8ff!8m2!3d37.789545!4d-122.397592)

## What is it like?

1. [Day 2 of Erica Lindberg](https://www.youtube.com/watch?v=xrWR0uU4nbQ)
2. [Acquisitions, growth curves, and IPO strategies: A day at Khosla Ventures](https://about.gitlab.com/2019/04/08/khosla-ventures-gitlab-meeting/)
3. [GitLab CEO Shadow  Update - May 30, 2019](https://www.youtube.com/embed/EfBMu9dTpno)

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/EfBMu9dTpno" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->
4. [Key takeaways from CEO Shadow C Blake](https://youtu.be/3hel57Sa2EY)
 <!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/3hel57Sa2EY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->


## CEO shadow program alumni

| Start date | End date | Name | Title |
| ------ | ------ | ------ | ------ |
| 2019-03 | 2019-04 | Erica Lindberg | Manager, Content Marketing |
| 2019-04 | 2019-05 | Mayank Tahil | Alliances Manager |
| 2019-04 | 2019-05 | Tye Davis | Sr. Technical Marketing Manager |
| 2019-05 | 2019-06 | John Coghlan | Evangelist Program Manager |
| 2019-06 | 2019-06 | Cindy Blake | Sr. Product Marketing Manager |


