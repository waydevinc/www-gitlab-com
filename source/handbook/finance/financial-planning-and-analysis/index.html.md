---
layout: markdown_page
title: "Financial Planning & Analysis"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Financial Planning @ GitLab

GitLab creates an annual Plan which establishes our financial objectives for the coming fiscal year and is used for measurement of the Company's ability to achieve goals. Things move quickly and our forecast needs to iterate quickly to keep up with the business. Accordingly, we run a rolling 4 quarter forecast process. This means that we are always looking out a minimum of 12 months when projecting revenue and expenses, and we update those forecasts at least once per quarter.  We plan our expenses at a high level (e-group) and we expect this group to make prioritizations and trade-offs while remaining accountable against the plan parameters. By reforecasting quarterly, we can quickly evaluate and incorporate new initiatives into our forecasting model. That being said, we do follow an annual plan to set our goals and measurement for our top-level targets of revenue, profitability and expense management. We follow the cadence below in our planning process:

**Note: During the last quarter of the year, GitLab runs a 5 quarter forecast that aligns with it's annual planning efforts.**

## Creating & Updating Models

If you create and/or update to a model that materially impacts it, please make sure to make a quick note of what you did in the [ChangeLog](https://gitlab.com/gitlab-com/finance/blob/master/changelog.md). This is important because it keeps us on all the same page.   

## Forecasting Growth Using a Growth Persistence Model

Scale Venture Partners developed a [Growth Persistence Model](https://www.scalevp.com/blog/predictable-growth-decay-in-saas-companies) based on research on private and public software companies. The Persistence Model shows that ARR growth rates typically decline year over year, with each year’s growth being, on average 85% to that of the preceding year. We use this methodology to set our growth targets.

## Making Changes to the Plan to Optimize for Growth

Our Plan is intended to ensure that the Company has sufficiently considered what is required to support the Company's financial goals. The Plan ensures that there is accountability across all functions that can be measured. The Plan is not intended to represent a cap on what we can invest in support of maximizing the growth of the Company.  We encourage our team members to actively seek opportunities that will help us grow faster. Those growth initiatives will be evaluated, as follows:

1. If the investment will allow us to exceed our IACV plan by at least an equal amount in the current year we will do it. The e-group will be informed of the increased spend and anticipated impact.
1. If the increased spend will allow us to exceed our IACV targets next year then we will quantify the impact and raise our financial guidance for the following year. The e-group will review these investments and will, in some cases, request board approval for relief against current year planning targets.
1. If we need to exceed spend to achieve our current plan we should review as an e-group and inform the board. We won't necessarily get plan relief unless there are extenuating circumstances but we will do what is right for the business.

## Business Drivers

At GitLab, planning starts with long term goals. GitLab uses business drivers to forecast out long term goals by function. Business drivers are what drives GitLab’s operating model. By using function specific business drivers that tie into the long term goals, GitLab is able to quickly and dynamically shift priorities based on external or internal changes. GitLab’s business drivers can be found in the GitLab Financial Model under the tab `Business Drivers`, which link the Business Driver labels to the definitions in the respective handbook. [Business Drivers cited by CFI](https://corporatefinanceinstitute.com/resources/knowledge/modeling/business-drivers/)

Business drivers are the key inputs and activities that drive the operational and financial results of a business.[^3]

## Operating Model Diagram
![alt text](https://about.gitlab.com/handbook/finance/financial-planning-and-analysis/financial-workflow.png "Operating Model")

## Long Term Profitability Targets

Our long term profitability target for EBITA (Earnings Before Interest Taxes and Amortization) is 20% of revenue.
Our long term gross margin target is 85% but is dependent on the revenue mix between products - self managed and [gitlab.com](https://about.gitlab.com/handbook/product/#long-term-financial-success-criteria-for-gitlabcom) and [professional services](https://about.gitlab.com/handbook/customer-success/professional-service-engineering/#long-term-profitability-targets). Gross margin is defined as total revenue less cost of revenues as defined by GAAP and reported in the Company's financial statements.
Other financial planning targets are described in the functional area (e.g. sales, marketing, development, etc.) in this handbook. We plan to achieve our long term profitability target in FY25-Q4.

### Long Term Profitability Targets
The long term target for operating expenses as a percentage of revenue for G&A is 10%.


## FY20 Financial Planning Goals

| Function  | FY20 Expense Target  |
|---|---|
| Customer Support  |10% of ARR   |
| Customer Solutions  | 80% of PS Recognized Revenue  |
| Engineering  | 70% of IACV  |
| Sales  | 75% of IACV  |
| Marketing   | 45% of IACV  |
| G&A  | 10% of Total Operating Expense  |

****

## <i class="fas fa-users fa-fw icon-color font-awesome" aria-hidden="true"></i> FP&A Team Responsibilities

### Manager, Financial Planning & Analysis

###### Responsibilities

1. Coordinates Monthly Investor Update
1. Coordinates Rolling 4 Quarter Process
1. Coordinates Annual Planning Process
1. Ensures GitLab's financial model and forecast is accurate
1. Partners with Business Operations to ensure Financial Systems & Integration Roadmap
1. Develops data processes in partnership with the Data team
1. Responsible for financial and mechanical integrity of the operating model and street model

Note: Financial Planning & Analysis Lead performing Finance Business Partner role on interim basis for CEO, CPO, CFO and Head of Business Development.



[Position Description](/job-families/finance/finance-planning-and-analysis/){:.btn .btn-purple}

### Finance Business Partner

###### Responsibilities

1. Reviews BvsA Analysis with e-group member (or designee)
1. Responsible for leading the Rolling 4Q forecast process
1. Building integrated financial model with gearing ratios for business.
1. Coordinates Monthly KPI and OKR prep
1. Maintain and develop KPI definitions for functional business units
1. Develops and maintains cost and efficiency benchmarking
1. Develops and maintains Unit economic analysis
1. Partners with function leaders to ensure financial and key metric forecast accuracy of supported functions
1. Provides Adhoc Analysis to supported functional leaders
1. Helps business group negotiate Vendor Contracts
1. Develops and maintains compensation modeling


[Position Description - Engineering Specific](/job-families/finance/finance-business-partner-engineering/){:.btn .btn-purple}

[Position Description - Sales Specific](/job-families/finance/finance-business-partner-sales/){:.btn .btn-purple}

### Financial Analyst

###### Responsibilities

1. Reviews BvsA Analysis - G&A Function
1. Partners with Recruiting on hiring plan
1. Develops and maintains contract & renewal projections
1. Develops and maintains monthly audit projects
1. Develops and maintains planned vs actuals GitLab Team analysis
1. Reviews BambooHR to ensure GitLab Team are in the correct functions and departments

[Position Description](/job-families/finance/financial-analyst/){:.btn .btn-purple}

****

## Calendar of Events
Throughout the year the Executive Assistant team puts together a calendar of events for board members and other events associated with planning. Those events can be found in the [GitLab Board Calendar](https://docs.google.com/spreadsheets/d/1GW59GiT0MLXEgMxy2bN0ZVcbMs_wssOkP2c59f19YTk/edit?usp=sharing) sheet.

### Monthly Update
* Hold Departmental Metrics Meetings to review how well the departments are operating.
* Send out Investors Update
* Update our Budget vs Actual model with revenue, expense and headcount actuals.
* Perform an actuals vs  budget/forecast variance analysis.
* Distribute monthly results to budget owners.

###### Monthly Important Dates
* **10th of the Month** - Update Metrics Report
* **10th of the Month** - Update Revenue Model
* **10th of the Month** - Send out Investors Update
* **15th of the Month** - Update Financial Models
* **15th of the Month** - Distribute monthly results to budget owners

****

### Quarterly Forecast
* All of the activities in the Monthly Forecast.
* Budget owners update headcount planning templates and non-headcount expenses.
* Revenue model updated and signed off by CMO and CRO.

###### Quarterly Important Dates
* **14th of the Last Month in Fiscal Quarter** - Send out calender invites to review non-headcount & headcount expenses
* **22th of the Last Month in Fiscal Quarter** - Finalize non-headcount & headcount planning with departments
* **22th of the Month** - Finalize Revenue Model signoff from CMO and CRO

****

### Annual Plan
* All of activities above.
* Revise and update the annual sales compensation plan.
* Set annual quota assignments for revenue producing roles.
* Review product investments vs expected revenue generation.
* Set expected amount for annual compensation increases.
* Set targets for any contributors on a company based performance plan.
* Set company targets for board, investors and creditors.
* Our Annual Plan is viewable internally as a google slide presentation.  Search on "[current year e.g. 2018] Plan" to view.

###### Annual Planning Important Dates
* **14th of September** - Sales Compensation Direction
* **15th of September** - Four quarter rolling forecast kick-off
* **30th of September** - Four quarter rolling forecast completed
* **1st of November** - Preliminary outlook reviewed at  Board of Directors
* **6th of November** - Plan iteration and discussion at egroup offsite in Sonoma
* **30th of November** - Product Roadmap and Investments (board review)
* **6th of December** - Sales Ramp and Compensation (board review)
* **13th of December** - Marketing and Demand Generation (board review)
* **5th of January** - Sales compensation plans distributed to sales team
* **25th of January** - Plan sent to Board for Approval
* **31st of January** - 20xx Plan Approved by Board

### Terminology
* Plan is the term we used for the current plan of record which has been approved by the Board.  Typically this is set at the beginning of the year.
* Forecast is a dynamic assessment based on current expectations of financial performance.
* Target is a goal or objective that may be higher or lower than the Plan or Forecast. Targets are typically used in conjunction with setting OKRs, compensation plans or other performance objectives.
* Baseline is a measurement of actual expense or revenue that relates to a certain point in time (i..e month, quarter or year). We use baselines to measure progress of improvement against actual results. The progress can be stated in monthly, quarterly or annualized terms.
* Actuals are results that have been reported or exist in a system that is designated as a single source of truth for the item that is being measured.

### Out of Budget Business Case

In the event an organization is asking to spend money outside either the [Annual Plan](https://about.gitlab.com/handbook/finance/financial-planning-and-analysis/) or [Quarterly Forecast](https://about.gitlab.com/handbook/finance/financial-planning-and-analysis/), you should develop a business case showcasing what the spend is for and how Gitlab can benefit from spending it.  

This business case should be completed in conjunction with your [Finance Business Partner (FBP)](https://about.gitlab.com/handbook/finance/financial-planning-and-analysis/) to articulate the financial impact of the request.  Below are a few examples of the question you should be trying to answer:
* What is the purpose of the spend?
* How much is the spend?
* What kind of spend is it? (One time, contract, utility-based, etc..)
* What is the Return on Investment?
* If the ROI cant be measured what KPI are we trying to move? 
* Can we measure the impact in terms of ROI or KPI improvement?
* Have we investigated any other options?

Together with your FBP , this analysis should be presented to your leader for approval.

## Monthly Finance Planning Meeting

#### Purpose

Each month after the financials have been published, GitLab reviews department spend data in detail. The goal of this analysis is to compare department budgets with actual results and examine any material discrepancies between budgeted and actual costs. These costs are reviewed at the functional department level, allowing GitLab to measure progress in meeting its plan, forecast, and operating model. During the meeting, the Finance Business Partners will review GitLab results in addition to a detailed overview. Each function can expect to review the following during the monthly meetings: 

1. Company results
   -  Bookings IACV, TCV
   -  Spending OpEx vs Plan
1. Functional level review
  - Last month and last fiscal quarter (once per quarter)
1. Department level review
  - Last month and last fiscal quarter (once per quarter)
1. Review next 4 quarters vs Plan for the Function and for each department

#### Process
Following the month-end close, the Finance Business Partners will distribute department income statements to the related budget owners and the e-group members. Each department is then responsible for comparing these reports, which contain actual costs, to the budget. Departments, with guidance from the Finance Business Partners, should analyze their data and if necessary, discuss items of interest and take appropriate action. Any questions regarding the cost data should be discussed with the Finance Buisness Partner.

#### Timing
1. The Accounting Manager will send the income statements on or before the 15th of each month.
1. The Finance Business Parters will prepare a templated budget vs actual reporting package for distribution to the functional and departmental leaders. 
1. With guidance from the Finance Business Partners, each function and department leader will review the budget vs actuals in the same month.

#### Expense Controls and Improving Efficiency
1. The primary mechanism to ensure efficient spend of company assets is the approval process prior to authorization. See [Signature Authorization Matrix](/handbook/finance/authorization-matrix/).
1. The second mechanism is the budget vs actual review to determine reasons for variances vs Plan.

## Monthly Investor Update
Each month we send to our investors an update no later than the 10th day following the end of the month. For further reference see our [blog post](https://about.gitlab.com/2018/10/17/how-we-keep-investors-in-the-loop/).
#### Format
1. Thanks
1. Asks
1. Key Metrics
1. Lowlights
1. Highlights
1. Expectations (next month)

#### Process
1. On the 1st day of the month, the FinOps lead prepares a draft of the previous months investor update in a google document and posts in the #investor-update slack channel.
1. On the same day the draft is added to the #investor update slack channel, the FinOps lead will `@mention` e-group members with asks for topics related to the investor update agenda.
1. The e-group members will have no later than the 9th of each month to review and add input.
1. No later than the 10th of each month the will CEO send the update to the investor mailing list.
1. Once the investor update is sent to the investor mailing list, the FinOps lead will add the current investor update to the #investor-update slack channel, along with highlight commentary on GitLab's operating metrics.

## Department Structure

Below is a table showing the structure of GitLab departments as they exist in BambooHR and netsuite. Please [check out the Team Page](/company/team/org-chart) to see our org chart.

## By Function - Where GitLab team-members Report

| Sales                | Marketing           | Engineering      | Product            | G&A                 |
|:--------------------:|:-------------------:|:----------------:|:------------------:|:-------------------:|
| Business Development | Corporate Marketing | Meltano          | Product Mangement  | Business Operations |
| Channel              | Demand Generation   | Infrastructure   |                    | CEO                 |
| Commercial Sales     | Digitial Marketing  | Development      |                    | Finance             |
| Customer Solutions   | Field Marketing     | Quality          |                    | People Ops          |
| Customer Success     | Marketing Ops       | Security         |                    | Recruiting          |
| Enterprise Sales     | Product Marketing   | UX               |                    |                     |
| Field Operations     | Outreach            | Customer Support |                    |                     |


## Allocation Methodology

GitLab allocates cost items based on consumption of resources or headcount. Below are the workflow and diagrams that illustrate how various cost items are allocated:

### Workflow

#### Pre-Allocation P&Ls

The grid highlighted as `Pre-Allocation P&Ls` in the diagram below highlights what GitLab P&L structure would look like if there were no allocation methodology. The grid represents Cost Centers and Departments that fall within those Cost Centers. In order to support different dimensional P&Ls and ensure expenses are flowing to the proper Cost Center, GitLab applies an allocation methodology. Therefore, the `Pre-Allocation P&Ls` is meant to provide clarity and be a starting point to the allocation methodology process.

#### Allocation Methodology

The grid highlighted as `Allocation Methodology` in the diagram below highlights the specifc processes that occur in order to achieve proper expense allocations to their respective Cost Centers.

###### Step 1: Yellow Process

The allocation methodology is applied to the G&A Company Allocation department, Business Operations Program Spend, and the Recruiting department. Only items that are used by the entire company are captured in Business Operations Program spend (examples include gmail and slack). In addition to Business Operations Program Spend and Recruiting expenses, there are times GitLab needs a catch all account for one off expenses (examples Contribute, payroll service fees, etc). Those expenses are allocated in the same fashion as Business Operations Program Spend and Recruiting, but those expenses are recorded to the G&A Company Allocation department, before allocation.

1. Business Operations Program Expenses. All 6060 Spend
2. Recruiting Department
3. G&A Company Allocation department for all one off expenses.

**Note:** One thing to note during the Yellow Process is the Infrastructure expenses are removed. Infrastructure is allocated during the next phase of the allocation cycle. Therefore no shared expenses will hit the Infrastructure P&L.

###### Step 2: Red Process

The allocation cycle for GitLab's SaaS offering [GitLab.com](https://gitlab.com) starts. The Finance Business Parter for R&D maintains an allocation model to provide Accounting with an allocation breakout of expenses as well as a P&L based on allocation. The model can be found by searching My Drive > GitLab.com Model. The allocation for GitLab.com is broken out into two different methods.


1. Free vs Paid Users
   - Infrastructure
   - 3rd Party Expenses [i.e. Hosting Services]

1. Percentage of Revenue
   - Customer Support

- Allocation for Infrastructure and 3rd Party expenses are based off of usage from free, paid and internal users of GitLab.
- Allocation of Customer Support are based off of expected revenue from GitLab's self hosted and SaaS offerings.

These allocation breakouts will be documented in the GitLab.com model and will flow into the Post Allocation P&Ls. GitLab allocates these expenses to 3 Cost Centers

1. Marketing - Free users
1. R&D - Internal users
1. Cost of Sales - Paid users


###### Step 3: Blue Process

The Blue Process, which encapsulates the Yellow and Red process, is meant to bring the full allocation cycle together before the Post Allocation P&Ls are rendered.

#### Post Allocation P&Ls

The grid highlighted as `Pre-Allocation P&Ls` in the diagram below highlights the final process during the allocation cycle. As a result, new P&Ls are generated to show the breakout of the Infrastructure team across Cost Centers as well as GitLab.com.

### Diagram

![alt text](https://about.gitlab.com/handbook/finance/financial-planning-and-analysis/p&l.png "P&L Breakout")




### Creating New Departments
Successfully creating new departments require that various company systems be updated to capture newly defined department structures. Once the need for a new department arises, follow these steps:

1. Create an issue using the *dept_change* template.
2. In the issue, add the appropriate team members from each department included in the checklist.
3. Disclose the nature of the new department. Is the new department simply a name change, or there is a structural modification?   
4. Provide all detail necessary to ensure team members are assigned to the newly created departments.

