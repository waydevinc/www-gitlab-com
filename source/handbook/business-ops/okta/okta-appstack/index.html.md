---
layout: markdown_page
title: "Okta Application Stack"
---

## On this page
{:.no_toc}

- TOC
{:toc}

Below is the list of Applications currently configured in Okta. If you are an application owner for any of these applications that have incomplete or inaccurate information, please create a MR with the updated details and assign to @gitlab-rmitchell.

If you are an Application Owner of an Application not listed here please submit an [new application setup issue](https://gitlab.com/gitlab-com/gl-security/zero-trust/okta/issues/new?issuable_template=okta_add_application) for your application. We will work with you to verify details and provide setup instructions. 

## Okta Applications

### Baseline Entitlements for all Team Members

| Application | Business Purpose | Configured Access | Okta Application Admin | SAML/SWA/Other | Deployment Status | 
| --- | --- | --- | --- | --- | --- | 
| [BambooHR](https://about.gitlab.com/handbook/business-ops/tech-stack/#bamboohr) | HR Portal  | Everyone | Brittany Rohde | SWA | Complete for SWA. Needs SAML Conversion | 
| [Calendly](https://about.gitlab.com/handbook/business-ops/tech-stack/#calendly) |  Scheduling  | Everyone | TBA  | SWA | Complete |  
| [Carta](https://about.gitlab.com/handbook/business-ops/tech-stack/#carta) |  Share Options | Everyone | Paul Machle  | SWA | Complete |   
| [CultureAmp](https://about.gitlab.com/handbook/business-ops/tech-stack/#cultureamp) |  360 Review | Everyone | Trevor Knudsen | SWA | Complete |  
| [Expensify](https://about.gitlab.com/handbook/business-ops/tech-stack/#expensify) |  Expenses | Everyone | Wilson Lau | SWA/SAML | Complete for SWA. Needs SAML Conversion | 
| [G-Suite](https://about.gitlab.com/handbook/business-ops/tech-stack/#g-suite) |  Email/Calendar/Drive | Everyone | Robert Mitchell | SWA | Complete for SWA. Needs SAML Conversion |
| [GitLab](https://about.gitlab.com/handbook/business-ops/tech-stack/#gitlab) | GitLab.com for Team | Everyone | Robert Mitchell  | SAML | Complete |
| [Greenhouse](https://about.gitlab.com/handbook/business-ops/tech-stack/#greenhouse) | Recruitment  | Everyone| Erich Wegscheider | SWA/SAML | Complete for SWA. Needs SAML Conversion | 
| [Moo](https://about.gitlab.com/handbook/business-ops/tech-stack/#moo) | Business Cards  | Everyone | Trevor Knudsen  | SWA | Complete |
| [NexTravel](https://about.gitlab.com/handbook/business-ops/tech-stack/#nextravel) | Travel Booking | Everyone | Cassiana Gudgenov  | SWA | Complete |
| [Periscope](https://about.gitlab.com/handbook/business-ops/tech-stack/#periscope) | Data Analytics  | Everyone | Emilie Schario | SAML  | Incomplete, needs SAML Setup  |
| [Slack](https://about.gitlab.com/handbook/business-ops/tech-stack/#slack) | Chat/Collaboration  | Everyone | Robert Mitchell | SWA/SAML| Complete for SWA/WebApp. Needs SAML Conversion |
| [Will Learning](https://about.gitlab.com/handbook/business-ops/tech-stack/#will-learning) | HR Education  | Everyone  | Robert Mitchell  | SWA | Complete  |
| [Zoom](https://about.gitlab.com/handbook/business-ops/tech-stack/#zoom) |  VideoConferencing | Everyone | Robert Mitchell | SWA/SAML | Partially Complete, only support for Web Client. Needs SAML Updating for SSO |


### Other Applications

| Application | Business Purpose | Configured Access | Okta Application Admin | SAML/SWA/Other | Deployment Status | 
| --- | --- | --- | --- | --- | --- | 
| [ADP](https://about.gitlab.com/handbook/business-ops/tech-stack/#adp) |  Payroll (US) | Finance Team | Wilson Lau  | SAML | Incomplete. Needs configuration to enable access and understand use. |  
| Amazon Web Services | Cloud platform | TBA | TBA | Multiple options | Incomplete. Needs owner identification and configuration. |
| [Avalara](https://about.gitlab.com/handbook/business-ops/tech-stack/#avalara) | Taxation | Finance Department | Wilson Lau  | SWA | Complete |
| [Balsamiq Cloud](https://about.gitlab.com/handbook/business-ops/tech-stack/#balsamiq-cloud) | UX-Wireframing  | UX Department | Christie Lenneville | SWA | Complete |   
| Betterment | Financial Advice | US-Based Team Members | Brittany Rohde | SWA | Complete |
| [Blackline](https://about.gitlab.com/handbook/business-ops/tech-stack/#blackline) | Accounting Automation | Finance Department Members | Kim Stithem  | SWA/SAML | SWA Complete. Needs SAML Conversion |
| [Chorus](https://about.gitlab.com/handbook/business-ops/tech-stack/#chorus) | Call Management | Everyone (Available by Request) | JJ Cordz | SWA  | Complete | 
| [Clari](https://about.gitlab.com/handbook/business-ops/tech-stack/#clari) | Forecasting | Commercial and Enterprise Sales | Alex Tkach | SWA/SAML | SWA Complete. Needs SAML Conversion |  
| [ContractWorks](https://about.gitlab.com/handbook/business-ops/tech-stack/#contractworks) | Contract Management  | TBA | TBA  | SAML  | Incomplete - Needs Engagement with Owner |  
| [Cookiebot](https://about.gitlab.com/handbook/business-ops/tech-stack/#cookiebot) | Marketing | Marketing Dept | JJ Cordz | SWA  | Complete |  
| [Crowdin.com](https://about.gitlab.com/handbook/business-ops/tech-stack/#crowdincom) | Localisation  | Available by Request  | TBA | SWA  | Complete pending ownerID and team requirements | 
| dev.gitlab.com | Dev Platform | Available by Request | Amber Lammers | SWA | Complete |
| [DiscoverOrg](https://about.gitlab.com/handbook/business-ops/tech-stack/#discoverorg) |  Lead Generation | Marketing, Business Development, Commercial Sales Depts | JJ Cordz | SWA  | Complete | 
| [Disqus](https://about.gitlab.com/handbook/business-ops/tech-stack/#disqus) | Social Media  | Available by request | JJ Cordz  | SWA  | Complete |  
| Dribbble | UX Prototyping | UX Department | TBA | SWA | Complete |
| [Drift](https://about.gitlab.com/handbook/business-ops/tech-stack/#drift) | Conversations  | Marketing | JJ Cordz  | SAML  | Incomplete, pending SAML Config |  
| [Eventbrite](https://about.gitlab.com/handbook/business-ops/tech-stack/#eventbrite) | Event Management  | Field Marketing, Marketing Dept | JJ Cordz | SWA  | Complete |  
| [HackerOne](https://about.gitlab.com/handbook/business-ops/tech-stack/#hackerone) | Bug Bounty  | Security Dept | Robert Mitchell  | SAML  | Complete |   
| [LinkedIn Sales Navigator](https://about.gitlab.com/handbook/business-ops/tech-stack/#linkedin-sales-navigator) |   | Business Development Dept | JJ Cordz  | SWA  | Complete |   
| Lumity | US Health Benefits | US-based Team Members | Brittany Rohde | SWA | Complete |
| [MailGun](https://about.gitlab.com/handbook/business-ops/tech-stack/#mailgun) | Outbound Mail | TBA - Available to Request | Robert Mitchell | SWA  | Incomplete, need owner and group assignments |   
| [Marketo](https://about.gitlab.com/handbook/business-ops/tech-stack/#marketo) |  Marketing Automation | Marketing OPS  Dept| JJ Cordz  | SWA/SAML  | Complete for SWA. Needs SAML Conversion |  
| Modern Health | US Health Benefits | US-Based team members | Brittany Rohde | SWA | Complete |
| [Moz Pro](https://about.gitlab.com/handbook/business-ops/tech-stack/#moz-pro) |  SEO | Marketing Ops Dept | JJ Cordz  | SWA  | Complete  |
| [Netsuite](https://about.gitlab.com/handbook/business-ops/tech-stack/#netsuite) | Finances | TBA | TBA - Wilson Lau? | SAML  | Incomplete - needs Setup   |
| [Outreach.io](https://about.gitlab.com/handbook/business-ops/tech-stack/#outreachio) | Sales Engagement  | Commercial Sales, Enterprise Sales, Marketing | JJ Cordz | SWA  | Complete |
| [OwnBackup](https://about.gitlab.com/handbook/business-ops/tech-stack/#ownbackup) | SFDC Backup | Business Operations | Jack Brennan  | SWA | Complete, but need to confirm configuration |   
| [PagerDuty](https://about.gitlab.com/handbook/business-ops/tech-stack/#pagerduty) | Incident Response | InfraStructure, Customer Support, Security Departments  |  TBA | SAML | Incomplete, Need to identify Owner  |
| [Salesforce](https://about.gitlab.com/handbook/business-ops/tech-stack/#salesforce) | Sales CRM | Enterprise Sales, Commercial Sales, Marketing Dept | Jack Brennan | SWA/SAML/Custom  | Incomplete, needs SAML Configuration and Provisioning setup |
| [Sertifi](https://about.gitlab.com/handbook/business-ops/tech-stack/#sertifi) | eSign Application  | Everyone (Available by Request) | Alex Tkach?  | SWA | Yes |   |
| [Snowflake](https://about.gitlab.com/handbook/business-ops/tech-stack/#snowflake) | Data WareHouse  | Data Team (Available by Request) | Taylor Murphy | SAML | Complete  |
| [Sprout Social](https://about.gitlab.com/handbook/business-ops/tech-stack/#sprout-social) | Social  | Marketing OPS | JJ Cordz  | SWA/SAML | Complete for SWA. Needs SAML Conversion |
| [Staging.GitLab.com](https://about.gitlab.com/handbook/business-ops/tech-stack/#staging-gitlab) | GitLab Staging Site  | Infrastructure, Development Dept (Everyone can Request)  | Robert Mitchell  | SAML  | Complete  |
| Status.io | Status Monitoring | Infrastructure Dept | TBA | SWA | Complete. Need to confirm owner and groups |
| [Survey Monkey ](https://about.gitlab.com/handbook/business-ops/tech-stack/#survey-monkey) |  Surveys | Marketing and UX Dept | JJ Cordz | SWA  | Complete |
| TheHive | Security Operations | Security Ops team (Available on request) | Shane Peters | SWA | Complete |
| Tenable.io | Vulnerability Management | Security Dept | Anthony Carella | SAML | Incomplete, needs reconfiguration |
| [Tweetdeck](https://about.gitlab.com/handbook/business-ops/tech-stack/#tweetdeck) |  Twitter Management | Available on Request | JJ Cordz | SWA | Complete. Are there Shared Logins that we need to register? |
| [UsabilityHub](https://about.gitlab.com/handbook/business-ops/tech-stack/#usabilityhub) | UX Testing  | UX Department  | Sarah O’Donnell | SWA  | Complete  |
| [Xactly](https://about.gitlab.com/handbook/business-ops/tech-stack/#xactly) |  Sales Performance Managemetn | Finance Team | Matt Benzaquen  | SWA/SAML  | Complete for SWA. SAML Conversion and Entitlements need verification  |
| [YouTube](https://about.gitlab.com/handbook/business-ops/tech-stack/#youtube) |  Videos | Everyone can Request | JJ Cordz  | SWA | Complete. Any shared logins?  |
| [Zapier](https://about.gitlab.com/handbook/business-ops/tech-stack/#zapier) | Automations  | Everyone can Request  | TBA | SWA   | Complete. Need owner details.  |
| [Zendesk](https://about.gitlab.com/handbook/business-ops/tech-stack/#zendesk) | HelpDesk  | Customer Support and Security Dept | Lee Matos?  | SWA/SAML/Othert  | Incomplete. Needs SAML Configuration and Provisioning Setup  |
| [Zuora](https://about.gitlab.com/handbook/business-ops/tech-stack/#zuora) | Sales order processing | Sales Operations? | Wilson Lau  | SAML  | Incomplete, needs SAML Configuration  |


