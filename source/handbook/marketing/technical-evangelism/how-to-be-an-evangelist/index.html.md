---
layout: markdown_page
title: "How to be an Evangelist"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

### What is evangelism?
Sometimes referred to as advocacy, evangelism creates a human connection with buyers and consumers to technology way beyond typical content marketing, with a face and a name relaying the story, expressing an opinion, and ultimately influencing a decision. 

Many people believe Guy Kawasaki, the former chief evangelist of Apple Computer, to be the father of evangelism.

Sources: 
https://www.forbes.com/sites/theopriestley/2015/08/28/why-every-tech-company-needs-a-chief-evangelist/  
https://en.wikipedia.org/wiki/Evangelism_marketing  

### Who can be an evangelist?

**Everybody in the wider GitLab Community can be an evangelist.** Whether you work in marketing ops or infrastructure engineering, you have a point of view on the work you do and the ecosystem of open source enterprise technology. 

### Stages of evangelism
1. Beginner - when you start following the right accounts on Twitter, peruse HN regularly, and respond to people
2. Enthusiast - when you start creating content in the form of blog posts, videos, tweets, talks. Occasionally you create issues whenever you want to post something on the company blog or Medium publication.
3. Pro - when you are invited to give talks that have over 200 attendees, when your content sometimes goes viral, often snagging over 500 views. You regularly contribute to the GitLab blog and other community blogs, podcasts, other content channels.
4. Member of GitLab Influencer Board - when you are part of the GitLab Influencer run by [Emily Chin](gitlab.com/echin). You reach this stage for two reasons: you are a pro **and** there is a strategic benefit to GitLab to promote your thought leadership. 

### Benefits of being an evangelist
* Beginners: Joining the conversation about our ecosystem is a great way to develop a more nuanced perspective of your job. That leads to new and better ideas. Being aware of the various points of view in the industry also helps jumpstart the strategic brain juices which will help you contribute more deeply in work discussions. 

* Enthusiast: As an enthusiast you showcase your expertize in the public arena. This means you and your company benefit from the material and you start building name recognition that leads to new opportunities. This is also a good spot to be in if you want to pivot your career towards a new expertize.

* Pro: At this stage start being recognized as an expert. Folks want to hear from you and you get name recognition in the community. This can lead to job offers, promotions, etc.

* Member of the GitLab Influencer Board: At this point you are contributing to the GitLab strategy and story in the ecosystem. You are seen as a GitLab spokesperson and will get exposure at the largest venues. The Technical Evangelism team will help you write conference proposals and funnel opportunities to you.



