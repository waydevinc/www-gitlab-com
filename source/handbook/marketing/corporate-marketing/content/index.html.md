---
layout: markdown_page
title: "Content Marketing"
---

## On this page
{:.no_toc}

- TOC
{:toc}

The Content team includes audience development, editorial, social marketing, video production, and content strategy, development, and operations. The Content Marketing team is responsible for the stewardship of GitLab's audiences, users, customers, and partners' content needs. Content marketing creates engaging, inspiring, and relevant content, executing integrated content programs to deliver useful and cohesive content experiences that build trust and preference for GitLab.

Roles on the Content Marketing team include:

- [Content Editor and Managing Editor](/job-families/marketing/editor/)
- [Content Marketer](/job-families/marketing/content-marketing/)
- [Digital Production Manager](/job-families/marketing/digital-production-manager/)
- [Social marketer](/job-families/marketing/social-marketing-manager/)

## Quick links

- [Blog calendar](/handbook/marketing/blog/#blog-calendar)
- [Content marketing schedule](/handbook/marketing/corporate-marketing/content/schedule/)
- [Content Hack Day](/handbook/marketing/corporate-marketing/content/content-hack-day)
- [Editorial team page (including blog style guide)](/handbook/marketing/corporate-marketing/content/editorial-team)
- [GitLab blog](/handbook/marketing/blog)
- [Newsletters ](/handbook/marketing/marketing-sales-development/marketing-programs/#newsletter)
- [Social marketing handbook](/handbook/marketing/corporate-marketing/social-marketing/)
- [Social media guidelines](/handbook/marketing/social-media-guidelines/)
- [User spotlights](/handbook/marketing/corporate-marketing/content/user-spotlights)

## Content team responsibilities

- Content pillar production
- Customer content creation
- Video production
- Blog management including writing, editing, and scheduling
- Branded YouTube management
- Organic branded social media mangement (Twitter, Facebook, and LinkedIn)
- Social media campaigns

## Communication

**Chat**

Please use the following Slack channels:

- `#content` for general inquiries
- `#content-updates` for updates and log of all new, published content
- `#blog` RSS feed
- `#twitter` for questions about social
- `#content-hack-day` for updates and information on Content Hack Day

**Issue trackers**
 - [Blog](https://gitlab.com/gitlab-com/www-gitlab-com/boards/804552?&label_name[]=blog%20post)
 - [Content Marketing](https://gitlab.com/gitlab-com/marketing/corporate-marketing/boards/911769?&label_name[]=Content%20Marketing)
 - [Content by stage](https://gitlab.com/gitlab-com/marketing/corporate-marketing/boards/1122378?&label_name[]=Content%20Marketing)
 - [Digital production](https://gitlab.com/gitlab-com/marketing/corporate-marketing/boards/1120979?&label_name[]=Video%20project)
 - [Social marketing](https://gitlab.com/gitlab-com/marketing/corporate-marketing/boards/968633?&label_name[]=Social)

### Who to contact

Contact [Erica](/company/team/#EricaLindberg_), Manager, Content Marketing, for questions and information regarding the content team, strategy, processes, and schedule.

**[Blog & editorial](/handbook/marketing/corporate-marketing/content/editorial-team/)**
- [Rebecca](/company/team/#rebecca), Managing Editor
- Valerie, Senior Content Editor
- [Sara](/company/team/#sarakassabian), Content Editor

**Social media & video production**
- [Emily](/company/team/#emvonhoffmann), Social Marketing Manager
- [Aricka](/company/team/#arickaflowers), Digital Production Manager

**Content marketing**
- [Suri](/company/team/#suripatel), Content Marketing Manager, Dev
- [Chrissie](/company/team/#the_chrissie), Content Marketing Manager, Ops
- [Vanessa](/company/team/#_vanessaptri), Content Marketing Manager, Security

## Requesting content and copy reviews

1. If you are looking for content to include in an email, send to a customer, or share on social, check the [GitLab blog](/blog/) first.
1. If you need help finding relevant content to use, ask for help in the #content channel.
1. If the content you're looking for doesn't exist and it's a:
   1. Blog post: See the [blog handbook](/handbook/marketing/blog)
   1. Digital asset (eBook, infographic, report, etc.): open an issue in the [corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate-marketing/) and label it `content marketing`
   1. Video: open an issue in the [corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate-marketing/) and label it `video production`
1. If you need a copyedit, ping @erica. Please give at least 3 days' notice.

## Mission & vision

Our content marketing mission statement mirrors our [company mission](/company/strategy/#mission). We strive to foster an open, transparent, and collaborative world where all digital creators can be active participants regardless of location, skillset, or status. This is the place we share our community's success and learning, helpful information and advice, and inspirational insights.

Our vision is to build the largest and most diverse community of cutting edge co-conspirators who are defining and creating the next generation of software development practices. Our plan to turn our corporate blog in to a digital magazine will allow us to add breadth, depth and support to our participation in and coverage of this space.

## Content production

The content team supports many cross-functional marketing initiatives. We aim for content production to be a month ahead of planned events and campaigns so that content is readily available for use. In accordance with our values of [iteration](/handbook/values/#iteration) and [transparency](/handbook/values/#transparency), we publish our proposed content plans at the beginning of each quarter. We don't hoard content for one big launch and instead plan sets of content to execute on each quarter and published each piece of content as it's completed.

Content production is determined and prioritized based on the following:

1. Corporate GTM themes
1. Integrated campaigns
1. Corporate marketing events
1. Newsworthiness
1. Brand awareness opportunity

We align our content production to pillars and topics to ensure we're creating a cohesive set of content for our audience to consume. Pillar content is multi-purpose and can be plugged into integrated campaigns, event campaigns, and sales plays.

### Planning timeline

Pillar strategy is planned annually and reviewed quarterly. Content sets are planned quarterly and reviewed monthly. When planning, we follow the 80/20 rule: 80% of capacity is planned leaving 20% unplanned to react to last minute opportunities.

- **2 weeks prior to start of the quarter:** Proposed content plans are published to the content schedule. Product marketing, digital marketing, and corporate events marketing give feedback on the plan.
- **1 week prior to the start of the quarter:** Kickoff calls are held.
- **1st day of the quarter:** Content plans are finalized.
- **1st of each month:** Progress reviews are held. Plans are adjusted if needed.
- **Last day of the quarter:** Cross-functional retrospective is held.

### What is a content pillar?

A content pillar is a go to market content strategy that is aligned to a high-level theme (for example, Just Commit) and executed via sets. For example, "Just commit to application modernization" is a content pillar about improving application infrastructure in order to deploy faster. Within this pillar, many topics can be explored (CI/CD, cloud native, DevOps automation, etc.) and the story can be adapted to target different personas or verticals.  

We use content pillars to plan our work so we can provide great digital experiences to our audiences. The content team aligns to themes to ensure we are executing strategically and meeting business goals. Pillars allow us to narrow in on a specific topic and audience, and sets help us break our work into more manageable compontents. Each set created should produce an end-to-end content experience (awareness to decision) for our audience.

**Defintions**

- Theme: A high-level GTM message that doesn't change often. Themes are tracked as `Parent Epics`.  
- Pillar: A story within a theme. Pillars are tracked as `Child Epics`.
- Set: A topical grouping of content to be executed within a specific timeframe. Sets are tracked as `Milestones`.
- Resource: An informative asset, such as an eBook or report, that is often gated.

![Content pillar diagram](/images/handbook/marketing/corporate-marketing/content-pillar.png)

#### What's included in a content set?

Here's an example of what's included in a content set:

| Quantity | Stage | Content Type | DRI |
| ------ | ------ | ------ | ------ |
| 4 | Awareness | Thought leadership blog post | Content marketing |
| 1 | Awareness | Topic webpage | Content marketing |
| 1 | Awareness | Resource | Content marketing |
| 4 | Consideration| Technical blog post | Content marketing |
| 1 | Consideration | Whitepaper | Product & technical marketing |
| 1 | Consieration | Solution page | Content & product marketing |
| 2 | Consideration | Webcast | Product & technical marketing |
| 1 | Purchase | Demo | Technical marketing |
| 1 | Purchase | Data sheet | Product marketing |


**Sources:**

- [What Is a Content Pillar? The Foundation for Efficient Content Marketing](https://kapost.com/b/content-pillar/)
- [How to Create Pillar Content Google Will Love](https://contentmarketinginstitute.com/2018/04/pillar-content-google/)
- [What Is a Pillar Page? (And Why It Matters For Your SEO Strategy)](https://blog.hubspot.com/marketing/what-is-a-pillar-page)

#### Current pillars

##### Application modernization

Modernize application development to achieve automated deployment pipelines.

| ------ | ------ | ------ |
| 1. Organizations want to deploy faster and generate revenue through innovation but don’t have the architecture to support those goals. | 2. Legacy systems slow down development with complicated processes, open networks to security risks, and are difficult to maintain - leaving little room in IT budgets for innovating. | 3. Leveraging microservices and committing to a modernization strategy puts organizations on the path to development efficiency. |
| 4. Adopting Continuous Integration helps developers work faster and build better applications through automated testing. Errors are caught automatically so developers can focus on other tasks. | 5. Continuous Delivery/Deployment takes Continuous Integration a step further. New code is pushed more frequently through an automated release process, and developers can watch their work go live in minutes. | 6. Streamlining the toolchain and adopting cloud native architecture improves IT spend and increases efficiency by automating processes. Teams can use GitLab as the primary mechanism to configure, build, test, deploy, and manage their microservices. |

Topics: CI/CD, cloud native

##### Reduce cycle time

Improve software cycle time to deliver business value faster.

| ------ | ------ | ------ |
| 1. Every company is becoming a software company, increasing the demand for better software, faster. Reducing the time between thinking of an idea and having the code in production is vital to providing value to customers. | 2. If an organization is too slow to innovate, its competitors can rapidly absorb the market by becoming the first organization to meet the needs of customers.  | 3. Agile planning and DevOps delivery have demonstrated reliable and scalable solutions to streamline and accelerate application delivery. |
| 4. Teams use a variety of tools to automate integration, test code, manage digital assets, scan for vulnerabilities, and deploy/configure applications, but each tool adds a layer of complexity in a team’s workflow, so teams are forced to waste cycle time tinkering on tools and not delivering business value. | 5. To reduce cycle time, teams should look for ways to consolidate interrelated development activities. Development teams need a simple, comprehensive toolchain that can easily build, test, and deliver applications without the waste and overhead of managing dozens of disparate tools and integrations. | 6. Simplifying the toolchain empowers teams to focus on innovation and delivering value to customers faster. Organizations can use GitLab to address the challenges of rapidly building and delivering business applications to remain competitive. |

Topics: Agile delivery, value stream management

##### Secure apps

Application security for developers.

| ------ | ------ | ------ |
| 1. Applications have seen exponential growth in the frequency of attacks (and successful breaches) in recent years. The rise of cloud computing, containers, and the use of third party code, in addition to the need to ship quickly and frequently, have thrust security to the forefront of software development.Security can no longer be treated as an afterthought. | 2. Regulations like GDPR and massive attacks like WannaCry and NotPetya have made security a business ultimatum. This takes the code written to protect your business to the front line of commercial cyber defense. Secure apps, APIs, and data storage could mean the difference between a 20 year future or a three year future for your company. | 3. VPs of AppDev need to work with both their direct teams and broader organizations to establish a culture of security: Everyone should feel responsible for and empowered to make your product or service as secure as possible. Security needs to be top of mind for every project and iteration. |
| 4. To help developers achieve and abide by this mindset, it’s important that they are given the education and resources necessary to understand how and why it’s critical for them to consider security from the moment they write their first line of code. | 5. Shifting left is the answer: Building security in at the end is an inefficient and cost-inducing process. If developers start with security at the beginning of the SDLC, teams will find it easier to keep security tightly coupled with updates and iterations as they work within an agile or DevOps process/methodology. | 6. Usability must be top of mind when building security in from the start. Enabling developers with a tool like GitLab - which integrates vulnerability testing into the development process, keeping developers in the same tool and reducing workflow disruption - will be crucial in getting developer buy-in.  |

Topics: application security, devsecops

## Messaging for Verticals
When to develop vertical messaging: The key is to determine if an industry has a certain pain point that another industry does not share.  You need to describe the problem (using industry specific terminology, if necessary) and also how your product solves these problems for them. Additionally, you can create high-level messaging and then branch off; for example if multiple industries are very security conscious, create security focused marketing, and adapt to select high value verticals.
