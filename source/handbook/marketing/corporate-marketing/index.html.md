---
layout: markdown_page
title: "Corporate Marketing"
---

## Welcome to the Corporate Marketing Handbook
{:.no_toc}

The Corporate Marketing team includes Content Marketing, Corporate Events, PR, and Design. Corporate Marketing is responsible for the stewardship of the GitLab brand and the company's messaging/positioning. The team is the owner of the Marketing website and oversees the website strategy. Corporate Marketing develops a global, integrated communication strategy, executes globally, and enables field marketing to adapt and apply global strategy regionally by localizing and verticalizing campaigns for in-region execution. Corporate marketing also ensures product marketing, outreach, and marketing & sales development are conducted in a way that amplifies our global brand.
{: .note}

## On this page
{:.no_toc}

* Will be replaced with the ToC, excluding the "On this page" header
{:toc}

----

## Brand personality

GitLab's brand has a personality that is reflected in everything we do. It doesn't matter if we are hosting a fancy dinner with fortune 500 CIOs, at a hackathon, or telling our story on about.gitlab.com...across all our communication methods, and all our audiences, GitLab has a personality that shows up in how we communicate.

Our personality is built around four main characteristics.

1. Human: We write like we talk. We avoid buzzwords and jargon, and instead communicate simply, clearly, and sincerely. We treat people with kindness.
1. Competent: We are highly accomplished, and we communicate with conviction. We are efficient at everything we do.
1. Quirky: We embrace diversity of opinion. We embrace new ideas based on their merit, even if they defy commonly held norms.
1. Humble: We care about helping those around us achieve great things more than we care about our personal accomplishments.

These four characteristics work together to form a personality that is authentic to GitLab team-members, community, and relatable to our audience. If we were `quirky` without being `human` we could come across as eccentric. If we were `competent` without being `humble` we could come across as arrogant.

GitLab has a [higher purpose](/company/strategy/#mission). We want to inspire a sense of adventure in those around us so that they join us in contributing to making that mission a reality.

## Tone of voice


The following guide outlines the set of standards used for all written company
communications to ensure consistency in voice, style, and personality across all
of GitLab's public communications.

See [the Blog Editorial Style Guide](/handbook/marketing/corporate-marketing/content/editorial-team/#blog-style-guide) for more.

### About

#### GitLab the community

GitLab is an [open source project](https://gitlab.com/gitlab-org/gitlab-ce/)
with a large community of contributors. Over 2,000 people worldwide have
contributed to GitLab's source code.

#### GitLab the company

GitLab Inc. is a company based on the GitLab open source project. GitLab Inc. is
an active participant in our community (see our [stewardship of GitLab CE](/stewardship)
for more information), as well as offering GitLab, a product (see below).

#### GitLab the product

GitLab is a single application for the complete DevOps lifecycle. See the
[product elevator pitch](/handbook/marketing/product-marketing/#elevator-pitch)
for additional messaging.

### Tone of voice

The tone of voice we use when speaking as GitLab should always be informed by
our [Content Strategy](https://gitlab.com/gitlab-com/marketing/blob/master/content/content-strategy.md#strategy).
Most importantly, we see our audience as co-conspirators, working together to
define and create the next generation of software development practices. The below
table should help to clarify further:


<table class="tg">
  <tr>
    <th class="tg-yw4l">We are:</th>
    <th class="tg-yw4l">We aren't:</th>
  </tr>
  <tr>
    <td class="tg-yw4l">Equals in our community</td>
    <td class="tg-yw4l">Superior</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Knowledgeable</td>
    <td class="tg-yw4l">Know-it-alls</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Empathetic</td>
    <td class="tg-yw4l">Patronizing</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Straightforward</td>
    <td class="tg-yw4l">Verbose</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Irreverent</td>
    <td class="tg-yw4l">Disrespectful</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Playful</td>
    <td class="tg-yw4l">Jokey</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Helpful</td>
    <td class="tg-yw4l">Dictatorial</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Transparent</td>
    <td class="tg-yw4l">Opaque</td>
  </tr>
</table>

We explain things in the simplest way possible, using plain, accessible language.

We keep a sense of humor about things, but don't make light of serious issues or
problems our users or customers face.

We use colloquialisms and slang, but sparingly (don't look like you're trying too hard!).

We use [inclusive, gender-neutral language](https://litreactor.com/columns/5-easy-ways-to-make-your-writing-gender-neutral).

## Updating the press page

### Adding a new press release
1. Create a new merge request and branch in www-gitlab-com.
1. On your branch, navigate to `source` then `press` and click on the [`releases` folder](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/press/releases).
1. Add a new file using the following format `YYYY-MM-DD-title-of-press-release.html.md`.
1. Add the following to the beginning of your document:

```
---
layout: markdown_page
title: "Title of press release"
---
```

5. Add the content of the press release to the file and save. Make sure to include any links. It is important to not have any extra spaces after sentences that end a paragraph or your pipeline will break. You must also not have extra empty lines at the end of your doc. So make sure to check that when copy pasting a press release from a google doc.

### Updating the `/press/#press-releases` page

When you have added a press release, be sure to update the index page too so that it is linked to from [/press/#press-releases](/press/#press-releases).

1. On the same branch, navigate to `data` then to the [`press.yml` file](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/press.yml).
1. Scroll down to `press_releases:`, then scroll to the most recent dated press release.
1. Underneath, add another entry for your new press release using the same format as the others, ensuring that your alignment is correct and that dashes and words begin in the same columns.  
1. The URL for your press release will follow the format of your filename for it: `/press/releases/YYYY-MM-DD-title-of-press-release.html`.

### Updating the recent news section

1. Every Friday the PR agency will send a digest of top articles
1. Product marketing will update the `Recent News` section with the most recent listed at the top. Display 10 articles at a time. To avoid formatting mistakes, copy and paste a previous entry on the page, and edit with the details of the new coverage. You may need to search online for a thumbnail to upload to `images/press`, if coverage from that publication is not already listed on the page. If you upload a new image, make sure to change the path listed next to `image_tag`.
1. Move older content to the archive.

----

## Design

### Requesting design help

1. Create an issue in the corresponding project repository
    1. For tasks pertaining to [about.gitlab.com](/) create an issue in the [www-gitlab-com project](https://gitlab.com/gitlab-com/www-gitlab-com/issues).
    1. For all other marketing related tasks create an issue in the [corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues).
1. Add all relevant details, goal(s), purpose, resources, and links in the issue description. Also `@` mention team members who will be involved.
1. Set due date (if possible) — please leave at least 2 week lead time in order to generate custom design assets. If you need them sooner, ping @luke in the #marketing-design slack channel and we will make our best effort to accommodate, but can't promise delivery.
1. Add the `Design` and `Website Redesign` (if applicable) label(s) to your issue.

### The `Design` label in issue tracker

The `Design` label helps us find and track issues relevant to the Design team. If you create an issue where Design is the primary focus, please use this label.

### Project prioritization

Per the Design team's discretion, the prioritization of design projects will be based on the direct impact on Marketing.

To get a better sense of [corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues) prioritization, you can view the [Design Issue Board](https://gitlab.com/gitlab-com/marketing/corporate-marketing/boards/913023?&label_name[]=Design).

Design projects within the [www-gitlab-com project](https://gitlab.com/gitlab-com/www-gitlab-com/issues) can be tracked using the [Website](https://gitlab.com/gitlab-com/www-gitlab-com/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Website) label. The prioritization of projects for [about.gitlab.com](/) can be viewed on the [Website Issue Board](https://gitlab.com/gitlab-com/www-gitlab-com/boards/349137?milestone_title=No+Milestone&).

Any design requests that do not fall in line with the goals and objectives of Marketing will be given a lower priority and factored in as time allows.

### Design touchpoints

The Design team has a rather wide reach and plays a big part in almost all marketing efforts. Design touchpoints range from the [GitLab website](/) to print collateral, swag, and business cards. This includes, but certainly not limited to:

#### Web & Digital
{:.no_toc}
- Redesign, updates, and maintenance of the [GitLab website](/)
- Blog post covers & images
- Landing pages (campaigns, webcasts, events, and feature marketing)
- Swag shop (shop design and new swag)
- Presentation decks & assets
- Ad campaigns
- Email template design

#### Field Design & Branding
{:.no_toc}
- Marketing Design System
- Conference booth design
- Banners & signage
- Swag
- Event-specific slide decks
- Event-specific branding (e.g. GitLab World Tour, Team Summits, etc.)
- Business cards
- One-pagers, handouts, and other print collateral
- GitLab [Brand Guidelines](/handbook/marketing/corporate-marketing/design/brand-guidelines/)

#### Content Design
{:.no_toc}
- Promotional videos & animations
- Social media campaign assets
- Webcast collateral & assets
- eBooks
- Whitepapers
- Infographics & diagrams

*In the spirit of 'everyone can contribute' (as well as version control and SEO) we prefer webpages over PDFs. We will implement a `print.css` component to these webpages so that print PDFs can still be utilized for events and in-person meetings without the headache of version control*

## Brand Guidelines

To download the GitLab logo (in various formats and file types) check out our [Press page](/press/).

### The GitLab logo

The GitLab logo consists of two components, the icon (the tanuki) and the wordmark:

<img src="/images/handbook/marketing/corporate-marketing/design/gitlab-lockup.png" class="full-width">

GitLab is most commonly represented by the logo, and in some cases, the icon alone. GitLab is rarely represented by the wordmark alone as we'd like to build brand recognition of the icon alone (e.g. the Nike swoosh), and doing so by pairing it with the GitLab wordmark.

#### Logo safe space

Safe space acts as a buffer between the logo or icon and other visual components, including text. this space is the minimum distance needed and is equal to the x-height of the GitLab wordmark:

<img src="/images/handbook/marketing/corporate-marketing/design/x-height.png" class="full-width">

![Logo safe space](/images/handbook/marketing/corporate-marketing/design/logo-safe-space.png){: .medium.center}

![Logo safe space](/images/handbook/marketing/corporate-marketing/design/icon-safe-space.png){: .small.center}

The x-height also determines the proper spacing between icon and workdmark, as well as, the correct scale of the icon relative to the wordmark:

![Logo safe space](/images/handbook/marketing/corporate-marketing/design/stacked-logo-safe-space.png){: .small.center}

### The Tanuki

The [tanuki](https://en.wikipedia.org/wiki/Japanese_raccoon_dog) is a very smart animal that works together in a group to achieve a common goal. We feel this symbolism embodies GitLab's [mission](/company/strategy/#mission) that everyone can contribute, our [values](/handbook/values/), and our [open source stewardship](/company/stewardship/).

### GitLab trademark & logo guidelines

GitLab is a registered trademark of GitLab, Inc. You are welcome to use the GitLab trademark and logo, subject to the terms of the [Creative Commons Attribution Non-Commercial ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/). The most current version of the GitLab logo can be found on our [Press page](/press/).

Under the Creative Commons license, you may use the GitLab trademark and logo so long as you give attribution to GitLab and provide a link to the license. If you make any changes to the logo, you must state so, along with the attribution, and distribute under the same license.

Your use of the GitLab trademark and logo:

- May not be for commercial purposes;
- May not suggest or imply that you or your use of the GitLab trademark or logo is endorsed by GitLab, or create confusion as to whether or not you or your use of the GitLab trademark or logo is endorsed by GitLab; and
- May not suggest or imply or that you are affiliated with GitLab in any way, or create confusion as to whether or not you are affiliated with GitLab in any way.

Examples of improper use of the GitLab trademark and logo:

- The GitLab name may not be used in any root URL, including subdomains such as `gitlab.company.com` or `gitlab.citool.io`.
- The GitLab trademark and/or logo may not be used as the primary or prominent feature on any non-GitLab materials.

### Using other logos

Logos used on the about.gitlab.com site should always be in full color and be used to the specifications provided by the owner of that logo, which can usually be found on the owners website. The trust marks component found throughout the site is the only exception and should use a neutral tone:

<img src="/images/handbook/marketing/corporate-marketing/design/trust-marks.png" class="full-width">

The tanuki logo should also not have facial features (eyes, ears, nose...), it is meant to be kept neutral, but it can be accessorized.

### Colors

While the brand is ever-evolving, the GitLab brand currently consists of six primary colors that are used in a wide array of marketing materials.

##### Hex/RGB

![GitLab Hex/RGB Colors](/images/handbook/marketing/corporate-marketing/design/gitlab-hex-rgb-colors.png)

### Typography

The GitLab brand uses the Source Sans Pro font family. Headers (h1, h2, etc.) always have a weight of 600 (unless used in special situations like large, custom quotes) and the body text always has a weight of 400. Headers should not be given custom classes, they should be used as tags and tags alone (h1, h2, etc.) and their sizes or weights should not be changed, unless rare circumstances occur. Here are typography tags.

`H1: Header Level 1`

`H2: Header Level 2`

`H3: Header Level 3`

`H4: Header Level 4`

`p: Body text`

### Buttons

Buttons are an important facet to any design system. Buttons define a call to action that lead people somewhere else, related to adjacent content. Here are buttons and their classes that should be used throughout the marketing website:

**Note**: Text within buttons should concise, containing no more than 4 words, and should not contain bold text. This is to keep thing simple, straightforward, and limits confusion as to where the button takes you.

#### Primary buttons

Primary buttons are solid and should be the default buttons used. Depending on the color scheme of the content, purple or orange solid buttons can be used depending on the background color of the content. These primary buttons should be used on white or lighter gray backgrounds or any background that has a high contrast with the button color. They should also be a `%a` tag so it can be linked elsewhere and for accessibility. Buttons should also be given the class `margin-top20` if the button lacks space between itself and the content above.

<div class="flex-container flex-column flex-start margin-bottom20">
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn orange margin-top20">Primary Button 1</a>
  <pre class="highlight shell">.btn.cta-btn.orange</pre>
  <p>OR</p>
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn purple">Primary Button 2</a>
  <pre class="highlight shell">.btn.cta-btn.purple</pre>
</div>

#### Secondary Buttons

There will be times when two buttons are needed. This will be in places such as [our jobs page](/jobs/), where we have a button to view opportunities and one to view our culture video. In this example, both buttons are solid, but one is considered the primary button (orange), and the other is the secondary button (white). The CSS class for the solid white button is <br> `.btn.cta-btn.btn-white`.

<img src="/images/handbook/marketing/corporate-marketing/design/jobs-buttons-example.png" class="full-width">

This is the proper use of two buttons, both being solid, but different colors based on hierarchy. If the background is white or a lighter color that doesn't contrast well with a white-backgound button, a ghost button should be used as a secondary button, and should match in color to the primary button beside it as shown below:

<div class="buttons-container flex-start">
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn orange margin-top20">Primary Button</a>
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn ghost-orange margin-top20">Secondary Button</a>
</div>

<div class="buttons-container flex-start margin-bottom20">
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn purple margin-top20">Primary Button</a>
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn ghost-purple margin-top20">Secondary Button</a>
</div>

DO NOT: Do not use these ghost buttons styles as standalone buttons. They have been proven to be less effective than solid buttons [in a number of studies](https://conversionxl.com/blog/ghost-buttons/). They should only be used as a secondary button, next to a solid primary button that already exists. Here are the classes for the secondary buttons:

<div class="flex-container flex-column flex-start margin-bottom20">
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn ghost-orange margin-top20">Secondary Button 1</a>
  <pre class="highlight shell">.btn.cta-btn.ghost-orange</pre>
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn ghost-purple">Secondary Button 2</a>
  <pre class="highlight shell">.btn.cta-btn.ghost-purple</pre>
</div>

### Iconography

Icons are a valuable visual component to the GitLab brand; contributing to the overall visual language and user experience of a webpage, advertisement, or slide deck. The GitLab iconography currently consists of "label icons" and "content icons", each are explained in further detail below:

#### Label icons

Label icons are intended to support usability and interaction. These are found in interactive elements of the website such as navigation and [toggles](https://about.gitlab.com/pricing/).

![Label icons example](/images/handbook/marketing/corporate-marketing/design/label-icons-example.png){: .medium.center}

#### Content icons

Content icons are intended to provide visual context and support to content on a webpage; these icons also have a direct correlation to our illustration style with the use of bold outlines and fill colors.

A few examples include our [event landing pages](https://about.gitlab.com/events/aws-reinvent/) and [Resources page](https://about.gitlab.com/resources/).

<img src="/images/handbook/marketing/corporate-marketing/design/content-icons-example.png" class="full-width">

### Brand oversight

Occasionally the [old GitLab logo](https://gitlab.com/gitlab-com/gitlab-artwork/blob/master/archive/logo/fox.png) is still in use on partner websites, diagrams or images, and within our own documentation. If you come across our old logo in use, please bring it to our attention by creating an issue in the [Marketing](https://gitlab.com/gitlab-com/marketing/general/issues) issue tracker. Please include a link and screenshot (if possible) in the description of the issue and we will follow-up to get it updated. Thanks for contributing to our brand integrity!

### [GitLab Product UX Guide](https://docs.gitlab.com/ee/development/ux_guide/)

The goal of this guide is to provide written standards, principles and in-depth information to design beautiful and effective GitLab features. This is a living document and will be updated and expanded as we iterate.

### [GitLab Product UX Design Pattern Library](https://brand.ai/git-lab/primary-brand/)

We've broken out the GitLab interface into a set of atomic pieces to form this design pattern library. This library allows us to see all of our patterns in one place and to build consistently across our entire product.

## Design System

### Brand resources

- [GitLab icons](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/gitlab-logo) (web RGB & print CMYK)
- [GitLab logos](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/gitlab-wordmark) (web RGB & print CMYK)
- Print-ready [event one-pagers](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/print/event-one-pagers)
- Color palette and Typography can be found in the [Brand Guidelines](/handbook/marketing/corporate-marketing/marketing-design-system/brand-guidelines/)
- [Authorized Reseller GitLab Virtuoso Badge](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/authorized-resellers/gitlab-virtuoso-badge)

### Asset libraries

#### Icons

- [Line icons](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/presentation-decks/_general-assets/icons/line-icons) (.png)
- [Line icons](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/_resources/icons/svg) (.svg)
- [Icon illustrations](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/presentation-decks/_general-assets/icons/illustrated-icons) (.png)
- [Software Development Lifecycle](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/software-development-lifecycle/complete-lifecycle-icons/png)

#### Icon patterns

- [GitLab icon pattern](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/illustrations/icon-pattern)

#### Social media

- [Profile assets](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/social-media/profile-assets/png/assets)

### Templates

#### Presentation decks

- [General GitLab deck template](https://docs.google.com/a/gitlab.com/presentation/d/16FZd01-zCj_1jApDQI7TzQMFnW2EGwtQoRuX82dkOVs/edit?usp=sharing)
- [Group Conversation template](https://docs.google.com/a/gitlab.com/presentation/d/1JYHRhLaO9fMy1Sfr1WDnCPGv6DrlohlpOzs48VvmlQw/edit?usp=sharing)
- [GitLab pitch deck template](https://docs.google.com/a/gitlab.com/presentation/d/1LC1lT-gxpl1oUZ2InX4Oni9T4MfR0DFF0RLi4uNxBBQ/edit?usp=sharing)

----

## Speakers

##### For GitLab Team-members Attending Events/ Speaking    

- If you are interested in find out about speaking opportunities join the #CFP slack channel. Deadlines for talks can be found in the slack channel and in the master GitLab [events spreadsheet](https://docs.google.com/spreadsheets/d/16usWToIsD-loDQYpflaMiGTmERMYSieNj_QAuk5HBeY/edit#gid=1939281399).
- If you want help building out a talk, coming up with ideas for a speaking opportunity, or have a customer interested in speaking start an issue in the marketing project using the [CFP submissions template](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/new?issuable_template=CFPsubmission) and tag any associated event issues. Complete as much info as possible and we will ping you with next steps. We are happy to help in anyway we can, including public speaking coaching, and builing out slides.
- If there is an event you would like to attend, are attending, speaking, or have proposed a talk and you would like support from GitLab to attend this event the process goes as follows:
 1. Contact your manager for approval to attend/ speak.
 1. After getting approval from your manager to attend, [add](https://about.gitlab.com/handbook/marketing/corporate-marketing/#how-to-add-events-to-the-aboutgitlabcomevents-page) your event/ talk to the [events page](/events/) and submit merge request to Emily Kyle.
 1. If your travel and expenses are not covered by the conference, GitLab will cover your expenses (transportation, meals and lodging for days said event takes place). If those expenses will exceed $500, please get approval from your manager. When booking your trip, use our travel portal, book early, and spend as if it is your own money. Note: Your travel and expenses will not be approved until your event / engagement has been added to the events page.
 1. If you are speaking please note your talk in the description when you add it to the Events Page.
 1. If you are not already on the [speakers page](/events/find-a-speaker/), please add yourself.
 1. We suggest bringing swag and/or stickers with you. See notes on #swag on this page for info on ordering event swag.

##### Finding and Suggesting Speakers and Submitting to CFPs   

- Speaker Portal: a catalogue of talks, speaker briefs and speakers can be found on our [Find a Speaker page](/events/find-a-speaker/). Feel free to add yourself to this page and submit a MR if you want to be in our speaker portal and are interested in being considered for any upcoming speaking opportunities.
- If you have a customer interested in speaking start an issue in the marketing project using the CFP submissions template and tag any associated event issues. Complete as much info as possible and we will ping you with next steps. We are happy to help in anyway we can, including public speaking coaching.

----

## Corporate Events

### Mission Statement
* The mission of the Corporate Events Team is to:
    * Showcase the value and strengths of GitLab on all fronts
    * Deliver creative solutions to problems
    * Provide exceptional service
    * Build lasting and trusting vendor and internal relationships
    * Treat everyone like they are our most valued customer, including fellow GitLab team-members

### What does the corporate Events team handle?
  * **Sponsored events** (events with 1000+ attendees that also have a global audience. There are soem exceptions. There a handful smaller events that we handle due to the nature of the audience and the awearenss and thoughtleadership positions we are trying to build out as a company)
  * **Owned events**
     * GitLab Commit (our User Conference)
  * **Internal events** (Contribute-sized events)
      * GitLab Contribute (our internal company and community event)
      * Can also provide assistance with SKO's, Force Management planning, Rewards Travel. Must be above 75 people attending for corp events involvement. 

### Corporate Events Strategy/ Goals
  * **Brand**
    *  For Sponsored Events: Get the GitLab brand in front of 15% of the event audience. 40,000 person event we would hope to get 4,000+ leads (10%) and 5% general awareness and visibility with additional branding and activities surrounding participation.
    *  Human touches- Tracked by leads collected, social interactions, number of opportunities created, referrals, current customers met, and quality time spent on each interaction.
    *  Audience Minimum Requirements- volume, relevance (our buyer persona, thought leaders, contributors), reach (thought leaders?), and duration of user/ buyer journey considered.
  * **ROI**
    * Work closely with demand gen campaigns and field marketing to ensure events are driving results and touching the right audience.
    * Exceed minimum threshold of ROI for any events that also have a demand gen or field component- 5 to 1 pipe to spend within a 1-year horizon.
    * Aim to keep the cost per lead for a live event around $100.
  * **Thought Leadership and Education**

### How We Evaluate and Build Potential Events
All GitLab events must check at least drive two or more of the aims of our events below to be considered.
  - Brand awareness- we want to be a house hold name by 2020!
  - Build community
  - Gain contributors
  - Thought leadership
  - Help with hiring
  - Get new relevant leads/ drive ROI  
  - Educate possible buyers or users on our product or features
  - ABM
  - iACV
  - Marketplace positioning
  - Partnerships/ Alliances

In addition, corporate events must meet:
  * Audience minimum requirement of 1000+ attendees and...
  * Audience demographic requirements. We consider the balance of roles represented (contributor, user, customer, potential hires), and the Global reach of the audience. 
 
We also ask the following questions when assessing an event:
- How and where will this position us as a brand? 
- Does it drive business goals forward in the next quarter? Year?
- Is the event important for industry, thought leadership, or brand visibility? We give preference to events that influence trends and attract leaders and decision makers. We also prioritize events organized by our strategic partners.
- Will there be a GitLab speaker? We do not require a speaker slot in return for sponsorship but we do prioritize events where the audience will be hearing about GitLab - either from a GitLab team-member or a member of the wider GitLab community.
- What type of people will be attending the event? We prefer events attended by diverse groups of decision makers with an interest in DevOps, DevSecOps, Cloud Native, Kubernetes, Serverless, Multi-cloud, CI/CD, Open Source, and other related topics.
- Will we be able to interact with attendees? We stress events that provide opportunities for meetings, workshops, booth or stands to help people find us, and other interaction with attendees.
- Where will the event be held? We aim to have a presence at events around the globe with a particular focus on areas with large GitLab communities and large populations of support.
- What is the size of the opportunity for the event? We prioritize events based their potential reach (audience size, the number of interactions we have with attendees) and potential for ROI (also account for cycyle time).
- What story do we have to tell here and how does the event fit into our overall strategy, goals, and product dircetion?
- Do we have the bandwidth and resources to make this activity a success? Do we have the cycles, funds, collateral and runway to invest fully and make this as successful as possible? Event must be weighed against other current activity in region and department.

Suggested events will be subject to a valuation calculation- will it meet or exceed objectives listed above?

Please review our events decision tree to ensure Corporate Marketing is the appropriate owner for an event. If it is not clear who should own an event based on the [decision tree](https://docs.google.com/spreadsheets/d/1aWsmsksPfOlX1t6TeqPkh5EQXergt7qjHAjGTxU27as/edit?usp=sharing), please email events@gitlab.com.

### Event Scorecard
Each question above is graded on a scale of 0-2. We then tally the scores and assign the event to a sponsorship tier.

- Events scoring below 8 are not eligible for corporate sponsorship or financial support.
- Events scoring 10+ are given top priority for staffing, and resources.

| Criteria / Score   | 0 | 1  | 2 |
|----------------|---------------|---------------|----------------|
| Thought Leadership |  |  |  |
| Audience type |  |  |  |
| Attendee interaction |  |  |  |
| Location and Timing |  |  |  |
| Event Relevance/ Strategy |  |  |  |
| Brand Reach |  |  |  |
| Opportunity size/ Potential ROI |  |  |  |

We ask these questions and use this scorecard to ensure that we're prioritizing the GitLab's brand and our community's best interests when we sponsor events.

If you have questions, you can always reach us by sending an e-mail to `events@gitlab.com`.

### Event Execution
* For questions regarding GitLab Contribute ping contribute@gitlab.com
* For hosted events and sponsored events that meet criteria above ping start an issue with event suggestion in Corporate Marketing Project. There is a template for suggesting new event sponsorships.
* For more on event execution see FM Handbook on the [event lifecycle](https://about.gitlab.com/handbook/marketing/marketing-sales-development/field-marketing/#event-lifecycle).

##### How We Decide Who Attends Which Events?

* Corporate determines how many staffers are needed based on the number of tickets we have allocated and activities we have at said event.
* If the event is more enterprise-focused we try to send more marketing/ sales. Regional Sales Managers in partnership with FM select staffer based on who has the most potential contacts in the area or going to event.
* If the event is more user-focused we will lean towards sending more technical people to staff and fewer sales.
* We also check who is in the area who might be a good fit for interfacing with the audience.
* We lean towards those who might be thought leaders, specialists, or more social in that specific sector. Example: for KubeCon we will bring those who are Kubernetes experts on the team.
* We aim to bring minimal staff to keep costs and disruption to normal workflow low. We take into account what value everyone will provide as well as staffing balance. Please comment in the event meta issue and tag the DRI if you would like to, or would like to suggest someone else participate in an event. 
* All those attending will need their manager's approval. 
* If you have been approved by the DRI and your manager to help staff an event, all your travel will be included during the time fo the event/ expo days. You need to be onsite and ready to help out as soon as the first expo hall shift opens up and you may book travel any time after the expo hall closes. We will cover the night of lodging before the expo hall opens through to the night it closes. Any additional nights will need to be coverd by the individual. 
* Event staffing list will close 3 weeks before commencement of the event.
* To request SA staffing, open an issue in the Customer Success project - [SA Service Desk subproject](https://gitlab.com/gitlab-com/customer-success/sa-service-desk) - using the `Event Participation Request` template. The same should be done for Support team staffing. Open an issue in the support project requesting assistance.
* If you are not officially involved in the event as part of the sponsorship, we would still like to know you will be attending so we can include you in any activities we have going on. Please comment in the slack channel letting us know your plans to attend after obtaining approval from your manager or comment in the epic for the event.
 
### Corporate Event Execution Process
1. Start issue for potential event using "Corporate Event" template. Complete as much info as you have.
2. Event/ Suggestion evaluated on criteria above (this can take up to two weeks or more depending on scope of opportunity). 
3. If event approved for execution, DRI for event will start finance issue for contract review and signature. 
   a. Use contract template in Finance project. Follow instructions in template. Be sure to include:
     * ROI caluclation for said event in the final cost section 
     * Link to the Meta issue for reference
     * Don't forget to put counter signed contract in ContractWorks
     * Close issue 

To be done by DRI once contract signed
4. As soon as contract has been signed, DRI needs to change the status of the issue tag from "status: plan" to "status: WIP". This let's the designated event MPM know to begin their back end exicution. He/ She will add a check list of issues to be created and info they need to create said issues and process. They will also create the Epic and associate everything existing to epic. 
5. Complete MPM check list. Questions include
     * Landing page needed
     * Preevent email being sent?
     * Do we have speakers
     * Will Allianecs be involved?
     * Will we be hosting a Happy Hour, Party or Dinner?
     * Will we get leads
     * Campaigns to be created...
6.  DRI to Start Event planning issue, using "Corporate Event Planner Issue" template for tracking prgress towards execution. 
7.  Add Event to Events Cal and Events Page (see instructions below)
8.  Start checking off planner template. Some things to note as you go through process in template:
     * Once you select staffing, start a slack channel and invite those folks as well as anyone from FM or alliance that needs to be involved. Do not link anythign but the epic in the slack channel as it will be deleted after 90 days. 
     * Share the planning sheet in the slack channel for people to fill out their contact and tracel info. Instruct everyone to book travel and lodging ASAP and add to planning sheet. 
     * The planning sheet is used for everything from Travel, meeting setting, booth duty, speakers list, networking events, PR...
9.  Once the Epic created... (the Epic is the main hub for all event info. ALL ISSUES ASSOCIATED WITH THE EVENT MUST BE LINKED TO THE EPIC!)
     * Link to Meta Issue
     * Add the planning sheet
     * Link to landing page
     * Booth hours
     * Booth Number (shoudl be in epic name)
     * Any other highlevel info that will be relevant to everyone attending. 
10. If the event needs a speaker, start an issue with the speaker request issue template and tag tech evangelism/ Priyanka. 
11. Landing Pages for Events
     *  Corporate events uses landing pages built from the GitLab events page. The MPM will create an issue for content to be provided on that page (work woth Alliances/ PMM on copy) and it will need to be decided in collaboration with FM if we want a form on the landing page. (more info on this can be found in the MPM handbook)
12. Schedule
     1. Event kick off call (include all people involved in planning), around 2 months out from event.
     2. Final event check in meeting (include everyone attending, anyone from Alliances involved, PMM who created demos to review them with team) 
     3. Event post mortem (with all planners/ stakeholders) 
     4. Lead Upload slot (Find an hour (need more time for event with 3+ event lead list uploads) time slot on ops person's cal who will be handing event lead upload. Recommended this be done around 48 business hours after event close. This will ensure they have time, they are given a heads up and they can get it done in a timely manner.)
13. Copy needed
     1. Landing page copy 
     2. 3-4 weeks out email invite copy
     2. 1-2 weeks out post event copy
14. Social 
     * Start issue using the social request template for genral social awareness posts and any social ads that need to go out. 
     * You will need to provide images and copy for any social ads and framework for content on all other social posts, as well as a general launch cadence. Ping EVH in issue with any questions. 
15. Design
     1. Issue needed for booth design (in corporate marketing project). Tag Luke and provide design specs as well as due date. Give him as much notice as possible. 
     2. For the latest approved booth design/ messaging ping events@gitlab.com. For any content or major layout changes start an issue in corporate marketing and tag PMM and Luke/ Design.
15. Digital
     * Cordinate all digital marketing requests with your MPM and Matt N. See [requesting marketing campiagns in the handbook](https://about.gitlab.com/handbook/marketing/marketing-sales-development/online-marketing/#requesting-marketing-campaigns) for more info. 
16. Meeting Setting
     * Most corporate events will have an onsite meeting setting initiative tied to the event goals. 
     * The FM for the region the event is DRI for the ancillary field activities, like meeting setting and customer dinners. 
     * We are responsible for booking the meeting space and they help work with sales to fill the time slots. 
     * Work with the regional FM lead and the designated event MPM to decide on best plan of attack for meeting setting. 
     * If any execs are on site, all meetings with them should be cordinated through designated EA lead for that event.
     * We track meetings on the master event spreadsheet. This sheet will be locked 24 hours before event starts- people will only be able to make comments. If you need to request a change ont his doc take the FM lead. 
     * All one site meetings must have a [meeting prep doc](https://docs.google.com/document/d/1QZB8m99Lt5GVnBDcfST1otmA81uH6EMEMkF-1zhQ0q0/edit), which can be found in the planning sheet. Note, we share these prep docs witht he client. 
     * All leads gathered through meeting setting initiatives must be tracked in their own campaign. 
     * We generally like to provide a small gift (unedr $50) for anyone who takes a meeting with us. 
     * Find ouy more on [meeting setting in the FM handbook](https://about.gitlab.com/handbook/marketing/revenue-marketing/field-marketing/#scanning-best-practices)
17. Demos, booth decks, and documentation
     * Procuct marketing helps to make all displayed collateral at events. 
     * These are the standard [demos](https://about.gitlab.com/handbook/marketing/product-marketing/demo/) we use/ present at events. They should be preloaded on event iPads. 
     * If you need soemthing specific for a show, start an issue in the PMM project and tag Dan Gordon. They need at least 3 weeks to produce something custom. 
17. Swag- decide appropriate giveaway for this event and audience. Coordinate ordering with one of our prefereed swag vendors. 
     * Order extra storage at event if all swag will not fit in booth
     * Send tracking to how when required
18. Leads and Campaign setup
     * DRI responsible for pulling, cleaning and sharing ead list with MPM and ops within 24 hours of event close. Use temples for clean upprovided by MPM.
     * Each list will need its own campaign in SFDC for tracking purposes. Find out more on [event lists here](https://about.gitlab.com/handbook/marketing/revenue-marketing/field-marketing/#event-list).

(Note this planning list is not exhaustive- see planning issue template in corporate marketing project for most up to date list of tasks)

### Best Practices on site at a GitLab event
  * [Employee Booth Guidelines](https://about.gitlab.com/handbook/marketing/revenue-marketing/field-marketing/#employee-booth-guidelines)
  * [Scanning Best Practices](https://about.gitlab.com/handbook/marketing/revenue-marketing/field-marketing/#scanning-best-practices)

### How to add events to the [about.gitlab.com/events](/events/) page

In an effort to publicly share where people can find GitLab at events in person throughout the world, we have created [about.gitlab.com/events](/events).  This page is to be updated by the person responsible for the event. To update the page, you will need to contribute to the [event master.yml](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/events.yml).
If you need more information about our exact involment in an specific event please visit the marketing project in gitlab.com and search the name of the event for any realted issues. The "Meta" issue should include the most thorough and high level details about each event we are participating in. Place your event in the order in which it is happening. The list runs from soonest to furthest in the future. 
Save event images and headers here: Save images for featured events [here](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/images/events)

#### Details to be included (all of which are mandatory in order for your MR to pass the build):

- **Topic** - Name of the event you would like to add
- **Type** - Please choose one of the following: `Diversity`, `Conference`,
`MeetUp`, `Speaking Engagement`, `Webinar`, `Community Event` or `GitLab Connect`. Events cannot have more than one type. If more than one apply, choose the best. If you feel your event doesn’t fit in the below category, do not just manually add a type. Please reach out to events@gitlab.com  to suggest a new type of event.
- **Date** - Start date of event
- **Date ends** - Day event ends
- **Description** - Brief overview about event (can be taken from event homepage).
- **Location** - city, state,provinces, districts, counties (etc depending on country), country where event will take place
- **Region** - `NORAM`, `LATAM`, `EMEA`, `APAC`, or `Online`
- **Social tags** - hashtag for event shared by event host
- **Event URL** - homepage for event

#### Example

```
- topic: The Best DevOps Conference Ever
  type: Conference
  date: January 1, 2050
  date_ends: January 3, 2050 # Month DD, YYYY
  description: |
               The Best DevOps Conference Ever brings together the best minds in the DevOps land. The conference consists of 3 full days of DevOps magic, literally magic. Attendees will have the opportunity to partake in fire talks and moderated sessions. This is one you won’t want to miss.
  location: Neverland, NVR
  region: APAC
  social_tags: DEVOPS4LIFE
  event_url: https://2050.bestdevops.org
```

#### Template

```
- topic:
  type:
  date:
  date_ends:
  description:
  location:
  region:
  social_tags:
  event_url:
```
For featured events include:
```
featured:
    background: background/image/src/here.png
```

### How to add an event specific landing page linking from the [about.gitlab.com/events page](/events/)

For corporate tradeshows we will want to create an event specific page that links from the [about.gitlab.com/events](/events/) page. The purpose of this page is to let people know additional details about GitLab’s presence at the event, how to get in touch with us at the event, and conference sessions we are speaking in (if applicable).   

For smaller Field Marketing shows we use Marketo landing pages vs. the events yml. By doing this, the MPMs own the creation of these pages and they are the only ones who will have edit access to these pages. 

When to specifically use a Marketo landing page vs. the events yml: 
1. This is an event owned by Field Marketing 
1. The event cost the company less than $10,000 (or your country's equivalent)
1. We will be driving traffic to the marketo landing page for less than 1.5 months. 

Steps to take to create the new page:

1. Create new a new branch of the [www-gitlab-com project.](https://gitlab.com/gitlab-com/www-gitlab-com). - Branch name should be what event you’ve added.
1. From new Branch, navigate to `Data`, then to `events.yml`
1. Scroll down to the area where its date appropriate to add the event
1. Add event using instructions in [handbook](/handbook/marketing/corporate-marketing/#how-to-add-an-event-to-the-eventsyml)
1. To create the event specific page you need to add a subset of the following information:

	- **url:** - you make this up based on what you want the URL to be from about.gitlab.com
	- **header_background:** choose from an image already in the images folder or add your own image. If you do not know how to do this, please watch [this tutorial](https://drive.google.com/open?id=14wCjHZLbcUUDArGYBNeinsHAlET6ubMy).  
	- **header_image:** choose from an image already in the images folder or add your own. (optional- if you prefer not to include, remove field altogether)
	- **header_description:** what CTA would you like the person to do on the page
	- **booth:** booth number at event, if there is no booth number, then remove this line of code (optional)
	- **form:** code that tells the system to add the contact info form. Marketing Ops will provide you with this number. They need to create a specific form for each page associated with a campaign in sfc.
	- **title:** CTA for why someone would want to give their contact info. Also used in `contact:` to distinguish a header title.
	- **description:** additional info on why someone would want to give their contact info
	- **number:** Marketo form number - Marketing Operations will need to give this number to you. Under `form:`
	- **content:** all of the information in the example section is all optional based on your event. If its not needed, simply delete.
1. Please watch [this tutorial](https://drive.google.com/open?id=14wCjHZLbcUUDArGYBNeinsHAlET6ubMy) for additional help.

### Example

```
Giving the following data will give this event it's own dedicated page on about.gitlab.com, must provide a unique url.
  If it is text, it needs to be wrapped in "double quotes". This is so you can use characters like : and ' without breaking anything.

  url: aws-reinvent
  header_background: /images/blogimages/gitlab-at-vue-conf/cover_image.jpg
  header_image: /images/events/aws-reinvent.svg
  header_description: "Drop by our booth or schedule a time, we'd love to chat!"
  booth: 2608
  form:
      title: "Schedule time to chat"
      description: "Learn more about how GitLab can simplify toolchain complexity and speeds up cycle times."
      number: 1592
  content:
    - title: "How to can get started with GitLab and AWS"
      list_links:
        - text: "Simple Deployment to Amazon EKS"
          link: "/2018/06/06/eks-gitlab-integration/"
        - text: "GitLab + Amazon Web Services"
          link: "/solutions/aws/"
        - text: "Top Five Cloud Trends"
          link: "/2018/08/02/top-five-cloud-trends/"
        - text: "How Jaguar Land Rover embraced CI to speed up their software lifecycle"
          link: "/2018/07/23/chris-hill-devops-enterprise-summit-talk/"
    - title: "Let's Meet!"
      body: "Join us for a live demo on getting started with Auto DevOps on Nov 28th at 1pm to learn how Auto DevOps simplifies your deployment pipeline to accelerate delivery by 200%, improves responsiveness, and closes the feedback gap between you and your users."
  speakers:
    - name: "Josh Lambert"
      title: "Senior Product Manger, Monitor"
      image: /images/team/joshua.jpg
      date: "Tuesday, Nov 28"
      time: "1:00pm PT"
      location: "Booth 2608 at the expo floor in the Venitian"
      topic: "GitLab CI 101"
      description: "In this talk, we will review how simple it is to get started with GitLab's built in CI tool."
    - name: "Reb"
      title: "Solutions Architect"
      image: /images/team/reb.jpg
      date: "Tuesday, Nov 27"
      time: "2:00pm PT"
      location: "Booth 2608"
```

### Template

```
Giving the following data will give this event it's own dedicated page on about.gitlab.com, must provide a unique url. If it is text, it needs to be wrapped in "double quotes". This is so you can use characters like : and ' without breaking anything.

  url:
  header_background:
  header_image:
  header_description:
  booth:
  form:
      title:
      description:
      number:
  content:
    - title:
      list_links:
        - text:
          link:
        - text:
          link:
        - text:
          link:
    - title:
      body:
  speakers:
    - name:
      title:
      image:
      date:
      time:
      location:
      topic:
      description:
    - name:
      title:
      image:
      date:
      time:
      location:
```
---
## Swag

Also handled by Corporate Events Team.
* We aim to have our swag delight and/ or be useful. We want swag that is versatile, easy to store, and transport. As a remote company with employees in over 40 countries, our swag often has to go on miraculous journeys. With this in mind we try to ship things that are durable, light, and that will be unlikely to get stuck in customs.
* We aim to make small batch, limited edition and themed swag for the community to collect. Larger corporate events will have custom tanuki stickers in small runs, only available at their specific event. We also produce one regional specific sticker design per quarter.
* We aim to do swag in a way that doesn't take a lot of time to execute => self serve => [web shop](https://gitlab.myshopify.com/)

### Community/ External Swag Requests:
If you would like to get some GitLab swag for your team or event, email your request to sponsorships@gitlab.com (managed by [community advocacy team](https://about.gitlab.com/handbook/marketing/community-relations/community-advocacy/#expertises)). In your request please include the expected number of guests, the best shipping address, and phone number along with what kind of swag you are hoping for. The swag we have available can be found on our online store. Note: We recommend you request swag at least 4 weeks out from the event date or we may not be able to accommodate your request.

### Internal GitLab Swag Ordering:
* Event Swag (for FM and community): To request GitLab swag for an event you are attending see instructions below.
  * The event in questions must be 3 or more weeks away for all swag and material requests. Rush shipping is not an option.
  * NORAM Field marketing and Community Relations should email our contct at Nadel for event swag shipments. Let them know what you want, when and where you need it. They will send your parcel with a return shipping label to get any remaining items shipped back to their warehouse. We have a list of approved items with Nadel you can order from. Any new items must be approved by brand team for brand consistency- Nadel will email all final designs to brand team for approval. 
  * Not in Field marketing or Community? You can place small event swag orders by emailing sponsorships@gitlab.com. Include the date needed, shipping address and items/ volume desired. The request will be approved on the back end by the community team. All requests must be made 3+ weeks out. You can expect a response within 5 business days. 
  * Paper/ Print Collateral: In order to be [efficient](/handbook/values/#efficiency), we do not make custom print assets for events. The [GitLab one-page datasheet](/images/press/gitlab-data-sheet.pdf) and the [GitLab Capabilities Statement](/images/press/gitlab-capabilities-statement.pdf) are the only print assets that we should use for events.  For printed materials (one pager, cheat sheets) please email events@gitlab.com. Our paper products are produced by Moo.com.
  * We have an event kit with a [banner and table cloth](/images/events/GitLabPopupBoothMarch2019.pdf). Contact events@gitlab.com if you would like to borrow this setup.  You will be shipped this set along with a return label. 
  * For larger swag orders (stickers in a quantity of 100 or greater), do not go through the swag store but rather use our [Stickermule](https://www.stickermule.com/) account or ping dsumenkovic@gitlab.com. Include address, date needed and order quantity in request.
  * If you have any issues with your order please email events@gitlab.com with your concerns.
* GitLab team-member Swag- if you would like to order something from the GitLab swag shop we have a discount code you can use for 30% off (found in the channel description). Please see the swag slack channel to get code to be used in the [store](https://shop.gitlab.com/) at checkout. 
* We have specific shirts available for customer meetings. If you feel you need one of these shirts please email events@gitlab.com.

### Returning Swag to Warehouse
* If you have items that need to be returned to the warehouse please contact events@gitlab.com or find the FexEx account number in 1password to create a return label. Returns are only recommended if you have a very large number of items (50+) or a booth setup (banner, tablecloth, backdrop) that need to be returned.

### Swag for customer/ prospects
ATM a limited test group of SDR's have sendoso accounts and the ability to send physical swag, handwritten notes, and coffee giftcards. We are working to get more accounts rolled out ASAP. 
 * Anyonw with our access to a swag send account requesting swag for customers, prospects, candidates, partners... please add swag requests to swag channel in slack and someone on swag team will reply within 24-48 business hours with discount code for our shopify store. The standard amount we offer is $25 but we can offer any amount; please specify if you want an amount other than $25.  
* If you have questions on what is appropriate to send review [sending swag to customers parameters](https://gitlab.com/gitlab-com/sales/issues/144).
 * SA's, TAM's, and AE's should coordinate with their SDR to send swag to customers. We will be expanding accoutn access to these groups in the near future (Q2).
 * Each SDR with an ccount has a set budget of $50 to spend on sneding swag and gift cards monthly. Mid market and SMB reps have $100.
 * All sends are tracked in SFDC, in either the physical or coffee swag campaign.

* We have GitLab stationary/ note cards- leave note in swag slack channel of you would like a batch to send notes to use to send to prospects/ customers/ community members.

* _NOTE:_ Please keep in mind the [list of countries we do not do business in](/handbook/sales-process/images_sales_process/#export-control).

### Swag Providers We Use
* See [issue](https://gitlab.com/gitlab-com/marketing/general/issues/1554) for vendors we use and what we order from them.
* Please direct swag vendor suggestions to the Swag slack channel.

### New and Replenishment Swag Orders
Corporate handles the creating and ordering of all new swag. All swag designs should be run past design (Luke) for approval before going to production.
* If you need swag for an upcoming event complete the swag selection of the event template and corporate will be in touch on issue to complete request. Note: at least 6 weeks to produce anything new and 2-3 weeks to reorder current designs.
* Triggers are setup in Sendoso to remind our account admins when ballances and swag inventory is low. No need to ping anyone if you see inventory is low.
* Reordering of inventory for internal swag requests is done by corporate team. See section above on swag providers we use for items not peroduced by Sendoso.

### Suggesting new items or designs
* You can suggest new designs in the swag slack channel or more formally in an issue in the [swag project](https://gitlab.com/gitlab-com/swag_suggestions).