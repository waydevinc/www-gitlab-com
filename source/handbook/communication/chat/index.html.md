---
layout: markdown_page
title: "GitLab Communication Chat"
extra_js:
  - libs/moment.min.js
  - libs/moment-timezone-with-data.min.js
  - team-call.js
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction
At GitLab, Slack is critical to our communication with each other.  While it enables real-time communication, we also are careful to remain true to our asynchronous mindset, suggesting that GitLab team-members set "do not disturb" and not expect real-time answers from others all the time.

There are groups of channels that can help with various areas of GitLab.  This page speaks to a few subsets of those channel groups.

## Channels

### General Channels

There are a lot of general channels, just to name a few:

* **[`#company-announcements`](https://gitlab.slack.com/archives/general)**: General announcements and company-wide broadcasts
* **[`#questions`](https://gitlab.slack.com/archives/questions)**: Ask all of GitLab for help with any question you have... and then go document it after you find the answer :)
* **[`#thanks`](https://gitlab.slack.com/archives/thanks)**: Where GitLab team-members can [say thanks](/handbook/communication/#say-thanks) to those GitLab team-members who did a great job, helped a customer, or helped you!
* **[`#random`](https://gitlab.slack.com/archives/random)**:  Socialize and share random snippets with your fellow GitLab team-members.

### Channel Categories

#### Account Channels (a_)
These channels (prefixed with a `a_`) are for team members to collaborate and communicate regarding the account for a customer organization. In some cases, they are marked as private or we are connected to customer's own Slack workspace using Shared Channels.

Many customers that require coordination across GitLab teams have dedicated channels in slack to discuss that customer's needs internally. Those channels are all postfixed with `-internal`.

Commercial (Mid-Market and SMB) accounts have specific [guidelines](/handbook/customer-success/comm-sales/#customer-engagements) around having Slack channels with customers.

#### Feature Channels (f_)

These channels are for teams and GitLab team-members interested in a specific feature that exists in GitLab or is being built today!

**Examples**

* **[`#f_web_ide`](https://gitlab.slack.com/archives/f_web_ide)**: Share ideas and feedback about our [Web IDE](/2018/06/15/introducing-gitlab-s-integrated-development-environment/)
* **[`#f_ldap`](https://gitlab.slack.com/archives/f_ldap)**: Talk about our integration with [LDAP providers](https://docs.gitlab.com/ee/administration/auth/ldap.html).

#### Group Channels (g_)

Group channels (prefixed with a `g_`) correspond to a [DevOps Stage group](/company/team/structure/#stage-groups) and other [engineering departments and teams](/handbook/engineering/#engineering-departments--teams).

**Example**

* **[`#g_create`](https://gitlab.slack.com/archives/g_create)**: Channel for the [Create](/handbook/product/categories/#create) development group.
* **[`#g_geo`](https://gitlab.slack.com/archives/g_geo)**: Dedicated to the [Geo group](/handbook/engineering/dev-backend/geo/).

#### Location Channels (loc_)

These channels are used to help GitLab team-members who are in the same general region of the world to talk about get-togethers and other location-specific items. They are also instrumental if you're going to be visiting somewhere else in the world - there might be a GitLab team-member or two nearby! Feel free to join the channel temporarily to chat with GitLab team-members who live there, and see if there is an opportunity to leverage the [visiting grant](/handbook/incentives/#visiting-grant) to add value to your trip.

**Examples**

* **[`#loc_nashville`](https://gitlab.slack.com/archives/loc_nashville)**: Chat with the GitLab team-members in Nashville, Tennessee, USA
* **[`#loc_portugal`](https://gitlab.slack.com/archives/loc_portugal)**: A channel for GitLab team-members in the entire contry of Portugal!

#### Social Groups

In addition to weekly company group calls that bring us together each week, Gitlab also have Social Slack Groups. Social Groups are slack channels that bring team members together around common interests, hobbies or lifesyles. Think Tennis club, Gaming, Shell collectors, Movie buffs, Faith groups, Football. You can see a list of Social Slack Groups and their tags below. Feel free to join any that resonate with you. This is a non-exhaustive list, but if you don't see a group you'd love to see on the list, reach out to our diversity team and we'd love to explore your idea.

**Examples:**

* **[`#managers`](https://gitlab.slack.com/archives/managers)**: Share ideas, thoughts, and issues on all things to do with being a manager at GitLab.
* **[`#pizza_lovers`](https://gitlab.slack.com/archives/)**: Pizza lovers unite!
* **[`#fitlab`](https://gitlab.slack.com/archives/fitlab)**: Channel to discuss fitness and related topics.
* **[`#games`](https://gitlab.slack.com/archives/games)**: Discuss gaming (both tabletop and video). We also have a Discord linked in the topic.
* **[`#music`](https://gitlab.slack.com/archives/music)**: Share your current playlist or any interesting music you come across.
* **[`#lgbtq`](https://gitlab.slack.com/archives/lgbtq)**: Space for LGBTQ people and allies in GitLab to chat and support each other. 
* **[`#cooking`](https://gitlab.slack.com/archives/cooking)**: Share your favorite recipes, and brag about your homemade dishes. 
* **[`#dad_jokes`](https://gitlab.slack.com/archives/dad_jokes)**: Self-explanatory really, give us your best dad jokes!
* **[`#cats`](https://gitlab.slack.com/archives/cats), [`#dog`](https://gitlab.slack.com/archives/dog), [`#cute-animal-photos`](https://gitlab.slack.com/archives/cute-animal-photos)**: Show us your cutest pics of your pets! We love all pets, but we made special channels for cats and dog(s).
* **[`#mental_health_aware`](https://gitlab.slack.com/archives/mental_health_aware)**: Mental health is super important and we realize that. This is a place to discuss concerns you might be having or find someone to talk to if you're feeling alone. We aim to be supportive and available for all who need it.
