---
title: Git happens! 6 Common Git mistakes and how to fix them
author: Sam Beckham
author_gitlab: samdbeckham
author_twitter: samdbeckham
categories: engineering
image_title: '/images/blogimages/fix-common-git-mistakes.jpg'
description: "Whether you added the wrong file, committed directly to master, or some other mishap, we've got you covered."
tags: git
ee_cta: false
---

We all make mistakes, especially when working with something as complex as Git.
But remember, Git happens!

If you're brand new to Git, you can learn [how to start using Git on the command line](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html). Now, without any
further ado, here's how we can fix six of the most common Git mistakes.

## 1. Oops… I spelled that last commit message wrong

After a good few hours of coding, it's easy for a spelling error to sneak into your commit messages.
Luckily, there's a simple fix.

```bash
git commit --amend
```

This will open up your editor and allow you to make a change to that last commit message.
No one needs to know you spelled, "addded" with three "d"s.

<%= partial "includes/blog/content-newsletter-cta", locals: { variant: "a" } %>

## 2. Oops… I forgot to add a file to that last commit

Another common Git pitfall is committing too early. You missed a file, forgot to save it, or
need to make a minor change for the last commit to make sense. `--amend` is your friend
once again.

Add that missed file then run that trusty command.

```bash
git add missed-file.txt
git commit --amend
```

At this point, you can either amend the commit message or just save it to keep it the same.

## 3. Oops… I added a file I didn't want in the repo

But what if you do the exact opposite? What if you added a file that you didn't want to commit?
A rogue ENV file, a build directory, a picture of your cat that you accidentally saved to the wrong folder?
It's all fixable.

If all you did was stage the file and you haven't committed it yet, it's as simple as resetting that staged file:

```bash
git reset /assets/img/misty-and-pepper.jpg
```

If you've gone as far as committing that change, you need to run an extra step before:

```bash
git reset --soft HEAD~1
git reset /assets/img/misty-and-pepper.jpg
rm /assets/img/misty-and-pepper.jpg
git commit
```

This will undo the commit, remove the image, then add a new commit in its place.

## 4. Oops… I committed all those changes to the master branch

So you're working on a new feature and in your haste, you forgot to open a new branch for it.
You've already committed a load of files and now them commits are all sitting on the master branch.
Luckily, [GitLab can prevent you from pushing directly to master](/2014/11/26/keeping-your-code-protected).
So we can roll back all these changes to a new branch with the following three commands:

*Note: Make sure you commit or stash your changes first, or all will be lost!*

```bash
git branch future-brunch
git reset HEAD~ --hard
git checkout future-brunch
```

This creates a new branch, then rolls back the master branch to where it was before you made
 changes, before finally checking out your new branch with all your previous changes intact.

## 5. Oops… I made a spelling mistake in my branch name

The keen-eyed among you will notice a slight spelling error in my last example. It's almost
3:00 PM and I haven't had lunch yet, so in my hunger, I've named our new branch `future-brunch`.
Delicious.

We rename this branch in a similar way to how we rename a file with the `mv` command: by
 moving it to a new location with the correct name.

```bash
git branch -m future-brunch feature-branch
```

If you've already pushed this branch, there are a couple of extra steps required. We need to
delete the old branch from the remote and push up the new one:

```bash
git push origin --delete future-brunch
git push origin feature-branch
```

## 6. Oops… I did it again

This command is for when everything has gone wrong. When you've copy-pasted one too
many solutions from Stack Overflow and your repo is in a worse state than it was when you started.
We've all been there.

`git reflog` shows you a list of all the things you've done.
It then allows you to use Git's magical time-traveling skills to go back to any point in the past.
I should note, this is a last resort thing and should not be used lightly.
To get this list, type:

```bash
git reflog
```

Every step we took, every move we made, Git was watching us.
Running that on our project gives us this:

```bash
3ff8691 (HEAD -> feature-branch) HEAD@{0}: Branch: renamed refs/heads/future-brunch to refs/heads/feature-branch
3ff8691 (HEAD -> feature-branch) HEAD@{2}: checkout: moving from master to future-brunch
2b7e508 (master) HEAD@{3}: reset: moving to HEAD~
3ff8691 (HEAD -> feature-branch) HEAD@{4}: commit: Adds the client logo
2b7e508 (master) HEAD@{5}: reset: moving to HEAD~1
37a632d HEAD@{6}: commit: Adds the client logo to the project
2b7e508 (master) HEAD@{7}: reset: moving to HEAD
2b7e508 (master) HEAD@{8}: commit (amend): Added contributing info to the site
dfa27a2 HEAD@{9}: reset: moving to HEAD
dfa27a2 HEAD@{10}: commit (amend): Added contributing info to the site
700d0b5 HEAD@{11}: commit: Addded contributing info to the site
efba795 HEAD@{12}: commit (initial): Initial commit
```

Take note of the left-most column, as this is the index.
If you want to go back to any point in the history, run the below command, replacing `{index}` with that reference, e.g. `dfa27a2`.

```bash
git reset HEAD@{index}
```

So there you have six ways to get out of the most common Gitfalls.

Have some Git tips of your own? Let us know in the comments below, we'd love to hear them.

Photo by [Pawel Janiak](https://unsplash.com/photos/WtRuYJ2EPMA?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/search/photos/mistake?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
{: .note}
