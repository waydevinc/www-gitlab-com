---
layout: markdown_page
title: "webpack"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property     | Value |
|--------------|-------|
| Date Created | TBD |
| Date Ended   | TBD |
| Slack        | TBD |
| Google Doc   | TBD |
| Epic         | TBD |

## Background

GitLab has been using [webpack](https://webpack.js.org) to bundle its frontend assets for three years now, and it has enabled us to do some great things, but at the same time we are not taking full advantage of its strengths (e.g. [code splitting](https://webpack.js.org/guides/code-splitting/) and [performance improvements](https://twitter.com/TheLarkInn/status/1012429019063578624)) and we are letting it get in the way of developer productivity ([high resource consumption](https://gitlab.com/gitlab-org/gitlab-ce/issues/32893), [crashes](https://gitlab.com/gitlab-org/gitlab-development-kit/issues/458), [broken debug tooling](https://gitlab.com/gitlab-org/gitlab-ce/issues/46524), etc) by not adequately addressing issues as we scale. This working group aims to both fix the most glaring issues with our frontend build tooling and to re-align the frontend development guidelines to promote best practices for performance and maintainability using webpack's underutilized features.

## Business Goal

Increase engineer productivity by fixing and improving our frontend build tooling, and put both guidelines and automated processes in place to ensure best practices are followed for performance and maintainability.

## Exit Criteria

- Improve GitLab's development environment.
    - Reduce the GDK's node process overall memory requirements 30% by improving the webpack/sprockets build process, and put measures in place to track this consumption over time, ensuring it does not grow unchecked.
- Improve GitLab's overall frontend performance.
    - Implement targeted builds for modern and legacy browsers (defined as those which [do and do not support `<script type="module">`](https://jakearchibald.com/2017/es-modules-in-browsers/#nomodule-for-backwards-compatibility) respectively) with only the code transformations and polyfills needed by each target.
    - Document code-splitting policies and put together a training workshop for frontend engineers and upload to youtube.
    - Use webpack output stats to drive performance improvements
        - Determine which webpack output stats should be tracked and monitored (entrypoint bundle sizes, number of entrypoints, initial page load code coverage, duplicate modules across bundles)
        - Implement measures to track webpack output stats.
        - Prescribe limits to these metrics, and goals for reducing them.
        - Put CI jobs in place to warn developers when these increase and enforce best practices.

## Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Facilitator           | Clement Ho            | Frontend Engineering Manager   |
| Frontend Lead         | Mike Greiling         | Senior Frontend Engineer       |
| Member                | John Hampton          | Frontend Engineering Manager   |
| Member                | Lukas 'Eipi' Eipert   | Senior Frontend Engineer       |
| Member                | Nathan Friend         | Senior Frontend Engineer       |
| Member                | Tristan Read          | Frontend Engineer              |
| Member                | David 'DJ' Mountney   | Senior Distribution Engineer   |
| Member                | Walmyr Lima e Silva Filho | Senior Test Automation Engineer |
| Executive Stakeholder | Dalia Havens          | Director of Engineering        |
