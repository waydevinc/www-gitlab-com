---
layout: markdown_page
title: "Working Groups"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What's a Working Group?

Like all groups at GitLab, a [working group](https://en.wikipedia.org/wiki/Working_group) is an arrangement of people from different functions. What makes a working group unique is that it has defined roles and responsibilities, and is tasked with achieving a high-impact business goal. A working group disbands when the goal is achieved (defined by exit criteria) so that GitLab doesn't accrue bureaucracy.

## Roles and Responsibilities

| Role                  | Required                  | Responsibility                                                                                                                       |
|-----------------------|---------------------------|--------------------------------------------------------------------------------------------------------------------------------------|
| Facilitator           | Yes                       | Assembles the working group, runs the meeting, and communicates results                                                              |
| Executive Stakeholder | Yes                       | An Executive or [Senior Leader](/company/team/structure/#senior-leaders) interested in the results, or responsible for the outcome  |
| Functional Lead       | Yes (for each function)   | Someone who represents their entire function to the working group                                                                    |
| Member                | No                        | Any subject matter expert                                                                                                            |

**Guidelines**

* An executive sponsor is required, in part, to prevent proliferation of working groups
* A person should not facilitate more than one concurrent working group
* Generally, a person should not be a part of more than two concurrent working groups in any role
* It is highly recommended that anyone in the working group with OKRs aligns them to the effort

## Process

* Preparation
  * Create a working group page
  * Assemble a team from required functions
  * Create an agenda doc public to the company
  * Create a Slack channel (with `#wg_` prefix) that is public to the company
  * Schedule a recurring Zoom meeting
* Define a goal and exit criteria
* Gather metrics that will tell you when the goal is met
* Organize activities that should provide incremental progress
* Ship iterations and track the metrics
* Communicate the results
* Disband the working group

## Active Working Groups

* [Development Metrics](/company/team/structure/working-groups/development-metrics/)
* [Log Aggregation](/company/team/structure/working-groups/log-aggregation/)
* [Single Codebase](/company/team/structure/working-groups/single-codebase/)
* [gitlab-ui (CSS and Components)](/company/team/structure/working-groups/gitlab-ui/)
* [SOX PMO](/company/team/structure/working-groups/sox/)
* [GitLab.com Revenue](/company/team/structure/working-groups/gitlab-com-revenue)
* [Webpack (Frontend build tooling)](/company/team/structure/working-groups/webpack/)
* [Licensing and Transactions Improvements](/company/team/structure/working-groups/licensing-transactions-improvements/)
* [Self-managed Scalability](/company/team/structure/working-groups/self-managed-scalability/)
* [Engineering KPIs](/company/team/structure/working-groups/engineering-kpis/)
* [Telemetry](/company/team/structure/working-groups/telemetry/)

## Past Working Groups

* [GitLab.com Cost](/company/team/structure/working-groups/gitlab-com-cost/)
