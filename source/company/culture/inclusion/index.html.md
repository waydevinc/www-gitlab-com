---
layout: markdown_page
title: "Diversity and Inclusion"
---

![Our Global Team](/images/summits/2019_new-orleans_team.png){: .illustration}*<small>In May of 2019, our team of 638 GitLab team-members from around the world had our annual company trip.  This year it was New Orleans!</small>*

### Diversity and Inclusion at GitLab

Over 100,000 organizations utilize GitLab across the globe and we aim to have a team that is representative of our users.

Diversity and Inclusion is a fundamental body of work to the success of GitLab. We aim to make a significant impact in our efforts to foster an environment where everyone can thrive. We are designing a multidimensional approach to ensure that GitLab is a place where people from every background and experiences in life feel they belong and can contribute. Collaborative work is one major component of our work.  We truly aim to uphold a culture which embodies transparency, opportunity and open communication.

### GitLab's Definition of Diversity and Inclusion

Although Diversity and Inclusion are often used interchangeably and can also be defined in several ways, it is helpful to understand how to differentiate between the two.  Here at GitLab it is described in the following way:

* Diversity is the ability to be able to recognize, respect and value differences based on several diversity dimensions, such as race, gender, age, ethnicity, religion, national origin, disability, sexual orientation, etc.  It also includes an infinite list of individual unique characteristics and experiences, such as communication style, career path, life experiences, educational background, marital status, military experience, parental status and other variables that influence personal perspectives.  These life experiences form how we as individuals approach challenges and solve problems such as how we may react and think differently, make suggestions and decisions, etc. Diversity is also about diversity of thought.  Here at GitLab we recognize that having this is important to business performance and requires tapping into these unique perspectives so that we do have a diverse workforce where everyone feels that can show up as their full self at work each day.

* Inclusion is the ability to understand diversity and to incorporate inclusive actions regardless of diversity dimensions.  It is a connection of diverse perspectives to come together where innovation can take place. It is the power of GitLabbers all showing up for the common goals of the company and each other. 

### Values

Inclusive teams are naturally more engaged, collaborative and innovative. We aim to align [our values](/handbook/values/) to be reflective of our company wide commitment to fostering a diverse and inclusive environment.

In addition, the very nature of our company is to facilitate and foster inclusion. We believe in asynchronous communication, we allow flexible work hours. GitLab team-members are encouraged to work when and where they are most comfortable.

### Fully Distributed and Completely Connected

The GitLab team is fully distributed across the globe, providing our team the opportunity to connect with each others cultures, celebrations and unique traditions. We collaborate professionally and connect personally!
Our unique remote team opens our door to everyone. Candidates are not limited by geography and we [champion this approach](http://www.remoteonly.org/), to the extent that it’s possible, for all companies!

### GitLab team-member Data

Please see our [identity data](/company/culture/inclusion/identity-data).

### Inclusion at GitLab

Gitlab is growing significantly, making it imperative to continuously foster community and build [allyship](/handbook/communication/ally-resources/). Diversity and Inclusion are crucial to building a collaborative company wide culture. We aim to retain all great talent.

### What We Are Doing with Diversity and Inclusion at GitLab:

*  Inclusive interviewing - We are building a an inclusive workforce to support every demographic. One major component is ensuring our hiring team is fully equipped with the skills necessary to connect with candidates from every background, circumstance. We thrive to ensure our hiring team is well versed in every aspect of Diversity, Inclusion and Cultural competence. We are helping the unconscious become conscious. Our number one priority is a comfortable and positive candidate experience.

*  Inclusive benefits - We list our [Transgender Medical Services] and [Pregnancy & Maternity Care] publicly so people don't have to ask for them during interviews.

*  Inclusive language - In our [general guidelines](/handbook/general-guidelines/) we list: 'Use inclusive language. For example, don't use "Hi guys" but use "Hi everybody", "Hi people", or "Y'all". And speak about courage instead of [aggression](https://www.huffingtonpost.com/2015/06/02/textio-unitive-bias-software_n_7493624.html). Also see the note in the [management section of the leadership page](/handbook/leadership/#management-group) to avoid military analogies.

[Transgender Medical Services]: /handbook/benefits/inc-benefits-us/#sts=Transgender%20Medical%20Services
[Pregnancy & Maternity Care]: /handbook/benefits/inc-benefits-us/#pregnancy--maternity-care

### What We Are Working on with Diversity and Inclusion:

*  Creating a Global Diveristy and Inclusion Team - A team of company influencers who can be instrumental in driving D&I efforts from a global perspective.
*  Empower employees with Employee Resource Groups based on diversity dimensions
*  Creating and environment where all voices can be hear and feel comfortable speaking
*  Ensuring equal access to opportunities
*  GitLab Safe Spaces - team wide opportunities for GitLab team-members to share perspectives, concerns and different outlooks

### Employee Training and learning opportunities

*  Inclusive interviewing
*  Coaching for inclusion
*  Conflict resolution
*  Understanding Unconscious bias
*  Psychological workplace safety

### Community

GitLab team-members are distributed across the globe, giving us access to an array of opportunity. We encourage collaboration with global organizations and programs that support underrepresented individuals in the tech industry. GitLab also provides additional support through the Diversity Sponsorship program. [GitLab Diversity Sponsorship program](/community/sponsorship/). We offer funds to help support the event financially and, if the event is in a city we have a GitLab team-member, we get hands-on by offering to coach and/or give a talk whenever possible.

### Mentorship

GitLab team-members can benefit from a [mentoring program](https://docs.google.com/forms/d/e/1FAIpQLSc68PSMge0iVoDoVU19n0JycHMMkVpq55gXj59ykdJJkcM_rg/viewform) because they will contribute to the development of a better-trained and engaged community.
Mentors will help mentees learn the culture, develop relationships across the organization, learn how to navigate throughout Gitlab and identify skills that could be strengthened.


#### Additional Resources and Training Opportunities

   * [Delivering Through Diversity](https://www.mckinsey.com/~/media/McKinsey/Business%20Functions/Organization/Our%20Insights/Delivering%20through%20diversity/Delivering-through-diversity_full-report.ashx) McKinsey and Company research on Diversity and it's value.
   * [Salesforce Equality at Work Training](https://trailhead.salesforce.com/trails/champion_workplace_equality).
 To earn badges and save your responses, you'll need to sign up! Use your GitLab address to sign in using Google+.
   * [Business Value of Equality](https://trailhead.salesforce.com/trails/champion_workplace_equality/modules/workplace_equality_diversity_and_inclusion) (This module has three units. The third is specific to Salesforce values and mission and is not required or suggested for our training.)
   * [Impact of Unconscious Bias](https://trailhead.salesforce.com/en/trails/champion_workplace_equality/modules/workplace_equality_inclusion_challenges)
   * [Equality Ally Strategies](https://trailhead.salesforce.com/trails/champion_workplace_equality/modules/workplace_equality_ally_strategies)
   * [Inclusive Leadership Practices](https://trailhead.salesforce.com/trails/champion_workplace_equality/modules/inclusive-leadership-practices)
   * To be truly inclusive is to be aware of your own ingrained biases as well as strategies for stopping the effects of those biases.  As part of our efforts, we recommend everyone to partake in [the Harvard project Implicit test](https://implicit.harvard.edu/implicit/takeatest.html) which focuses on the hidden causes of everyday discrimination.

### Never done

We recognize that having an inclusive organization is never done. If you work at GitLab please consider joining our #inclusion chat channel. If you don't work for us please email cwilliams@gitlab.com our Diversity and Inclusion Partner with suggestions and or concerns.

### Internal To GitLab and Want to Learn More?
*  Have questions or suggestions for diversity and inclusion?  Please email diversityinclusion@gitlab.com
*  Monthly D&I Initiaves Company Call.  This call will allow time for GitLabbers to gain an understanding of what we are doing with D&I.  This call is the second Wednesday of every month @10am EST.
*  D&I Office Hours - This call will allow time for questions, suggestions and themed topics to discuss.  Currently this call is bi-weekly but will move to monthly starting in August.  The next call will be 6/25 10am EST.  Both calls can be dialed into using the following zoom link: https://gitlab.zoom.us/j/7864690288
